module BalentsFisherGirvin

using LatticeTools
using QuantumHamiltonian

include("Kagome.jl")

function generate_basis(kagome, ::Type{BR}=UInt) where {BR<:Unsigned}

    n_triangles = length(kagome.nearest_neighbor_triangles)
    n_sites = numsite(kagome.lattice.supercell)

    membership = Dict(i => Set{Int}() for i in 1:n_sites)
    for (ihexagon, hexagon) in enumerate(kagome.motifs.hexagons)
        for (i, R) in hexagon
            push!(membership[i], ihexagon)
        end
    end

    allowed_ranges = [(0, 1), (0, 2), (0, 3), (1, 3), (2, 3), (3, 3)]

    function allowed(config::BR, i_site::Integer)
        lo_count = Tuple{Int, Int}[]
        for ihexagon in membership[i_site]
            lo_site_count, lo_magnon_count = 0, 0
            for (j, _) in kagome.motifs.hexagons[ihexagon]
                if j < i_site
                    lo_site_count += 1
                    lo_magnon_count += Int((config >> (j-1)) & 0x1)
                end
            end
            push!(lo_count, (lo_site_count, lo_magnon_count))
        end

        return (
            state
                for state in 0:1
                if let
                    compatible = true
                    for (lo_site_count, lo_magnon_count) in lo_count
                        minval, maxval = allowed_ranges[lo_site_count + 1]
                        val = lo_magnon_count + state
                        if !(minval <= val <= maxval)
                            compatible = false
                            break
                        end
                    end
                    compatible
                end
        )
    end

    function dfs(config::BR, i_site::Integer)::Vector{BR}
        if i_site > n_sites
            return [config]
        end

        out = BR[]
        for state in allowed(config, i_site)
            newconfig = config | (BR(state) << (i_site-1))
            append!(out, dfs(newconfig, i_site+1))
        end
        return out
    end

    return sort(dfs(zero(BR), 1))
end

function generate_hamiltonian(kagome, ::Type{BR}=UInt) where {BR<:Unsigned}
    n_triangles = length(kagome.nearest_neighbor_triangles)
    n_sites = numsite(kagome.lattice.supercell)

    up_membership = zeros(Int, n_sites)
    down_membership = zeros(Int, n_sites)

    for (itri, tri) in enumerate(kagome.motifs.up_triangles)
        for (isite, _) in tri
            up_membership[isite] = itri
        end
    end
    for (itri, tri) in enumerate(kagome.motifs.down_triangles)
        for (isite, _) in tri
            down_membership[isite] = itri
        end
    end


    term_signs = Dict(
        "A" => Dict("B" => true, "C" => false),
        "B" => Dict("C" => true, "A" => false),
        "C" => Dict("A" => true, "B" => false),
    )

    (hilbert_space, spin_operator) = QuantumHamiltonian.Toolkit.spin_system(n_sites, 1//2, BR)
    #hamiltonian = NullOperator()
    hamiltonian_terms = []
    for i_site in 1:n_sites
        (site_type, _) = getsitename(kagome.lattice.supercell, i_site)
        up_tri   = kagome.motifs.up_triangles[ up_membership[i_site] ]
        down_tri = kagome.motifs.down_triangles[ down_membership[i_site] ]

        target_sites = Int[]
        target_signs = Bool[]

        for (j_site, _) in up_tri
            if j_site == i_site
                continue
            end
            (nn_site_type, _) = getsitename(kagome.lattice.supercell, j_site)
            push!(target_sites, j_site)
            push!(target_signs, term_signs[site_type][nn_site_type])
        end

        for (j_site, _) in down_tri
            if j_site == i_site
                continue
            end
            (nn_site_type, _) = getsitename(kagome.lattice.supercell, j_site)
            push!(target_sites, j_site)
            push!(target_signs, term_signs[site_type][nn_site_type])
        end
        #@show target_sites, target_signs

        let
            push!(
                hamiltonian_terms,
                prod(
                    spin_operator(j_site, is_plus ? (:+) : (:-) )
                        for (j_site, is_plus) in zip(target_sites, target_signs)
                )
                # prod(
                #     spin_operator(j_site, is_plus ? (:-) : (:+) )
                #         for (j_site, is_plus) in zip(target_sites, target_signs)
                # )
            )
        end
    end

    return hamiltonian_terms

end

end # module
