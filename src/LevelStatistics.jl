

function get_spacings(values::AbstractVector{<:Real}, digits::Integer=8)
    values2 = round.(sort(values); digits=digits)
    spacings = values2[2:end] - values2[1:end-1]
    return spacings
end

function get_ratios(values::AbstractVector{T}, digits::Integer=8) where {T<:AbstractFloat}
    spacings = get_spacings(values, digits)
    n = length(spacings)
    n < 2 && return T[]

    m = n
    while m > 1 && spacings[m] == 0
        m -= 1
    end

    m == 1 && return T[]

    s_prev = spacings[m] # nonzero
    ratios = T[]
    while m > 1
        m -= 1
        s = spacings[m]
        r = min(s, s_prev) / max(s, s_prev)
        if s != 0
            s_prev = s
        end
        push!(ratios, r)
    end
    return ratios
end

function pdf_general(beta::Real)
    function pdf(x::Real)
        return (27.0/4) * (x+x^2)^(beta) / (1+x+x^2)^(1+3*beta/2)
    end
    return pdf
end


pdf_goe = pdf_general(1)
pdf_poisson(x::Real) = 2 / (1+x)^2
