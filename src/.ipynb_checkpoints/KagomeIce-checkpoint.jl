module KagomeIce

export my_metafmt
include("Preamble.jl")

include("Kagome.jl")
include("BalentsFisherGirvin.jl")

export get_spacings, get_ratios
include("LevelStatistics.jl")

end
