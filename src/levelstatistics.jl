function get_spacings(values; digits::Integer=8, nondegenerate::Bool=false)
    values2 = round.(sort(values); digits=digits)
    for (i, x) in enumerate(values2)
        if iszero(x)
            values2[i] = zero(x)
        end
    end
    nondegenerate && unique!(values2)
    length(values2) < 2 && return Float64[]
    return values2[2:end] - values2[1:end-1]
end


function get_ratios(values; digits::Integer=8, nondegenerate::Bool=false)
    spacings = get_spacings(values, digits=digits, nondegenerate=nondegenerate)
    n = length(spacings)
    n <= 3 && return Float64[]

    m = n
    while (m > 1) && iszero(spacings[m])
        m -= 1
    end
    m <= 1 && return Float64[]

    s_prev = spacings[m] # nonzero
    ratios = Float64[]
    while m > 1
        m -= 1
        s = spacings[m]
        r = min(s, s_prev) / max(s, s_prev)
        if !iszero(s)
            s_prev = s
        end
        push!(ratios, r)
    end
    return ratios
end


function pdf_general(beta::Real)
    function pdf(x::Real)
        return (27.0/4) * (x+x^2)^(beta) / (1+x+x^2)^(1+3*beta/2)
    end
    function pdf(x::AbstractArray{<:Real})
        return (27.0/4) .* (x + x.^2).^(beta) ./ (1 + x + x .^ 2)^(1 + 3 .* beta./2)
    end
    return pdf
end

pdf_goe = pdf_general(1)
pdf_poisson(x::Real) = 2 / (1+x)^2
pdf_poisson(x::AbstractArray{<:Real}) = 2 ./ (1+x).^2
