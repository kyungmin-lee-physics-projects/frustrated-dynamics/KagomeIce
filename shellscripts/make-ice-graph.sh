projectdir="${HOME}/Projects/KagomeIce"

srcdir="${projectdir}/src"
scriptsdir="${projectdir}/scripts"
datadir="${projectdir}/data"
plotsdir="${projectdir}/plots"

set -x

julia "${scriptsdir}/make-ice-graph.jl" "(2,0)x(0,2)"
julia "${scriptsdir}/make-ice-graph.jl" "(2,-2)x(2,4)"
julia "${scriptsdir}/make-ice-graph.jl" "(3,0)x(0,2)"
julia "${scriptsdir}/make-ice-graph.jl" "(4,0)x(0,2)"
julia "${scriptsdir}/make-ice-graph.jl" "(4,0)x(0,4)"
julia "${scriptsdir}/make-ice-graph.jl" "(3,-1)x(1,3)"

wait