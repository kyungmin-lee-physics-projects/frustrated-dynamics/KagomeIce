# Need to take Z2 into account

using DrWatson
quickactivate(joinpath(@__DIR__, ".."))

using Logging

using LinearAlgebra
using SparseArrays
using Printf
using DataStructures
using BitIntegers

import ArgParse
using Arpack

using LatticeTools
using QuantumHamiltonian
using KagomeIce

using CodecXz
using MsgPack
using Formatting


using PyCall
npl = pyimport("numpy.linalg")

function compute_kagome_bfg(
    Jx::Real, Jy::Real, Jz::Real,
    shape::AbstractMatrix{<:Integer},
    target_state_vec_list::AbstractVector{<:CartesianIndex},
    tsym_irrep_indices::AbstractVector{<:Integer},
    psym_irrep_indices::AbstractVector{<:Integer},
    psym_irrep_components::AbstractVector{<:Integer},
    max_dense::Integer, # nev::Integer,
    # measure::Bool,
    force::Bool=true
)
    @mylogmsg "lattice shape: $shape"
    @mylogmsg "Jx: $Jx"
    @mylogmsg "Jy: $Jy"
    @mylogmsg "Jz: $Jz"
    # @mylogmsg "Sz: $Szs"

    n11 = shape[1,1]
    n12 = shape[1,2]
    n21 = shape[2,1]
    n22 = shape[2,2]
    shape_str = "($n11,$n12)x($n21,$n22)"

    kagome = KagomeIce.make_kagome_lattice(shape)
    lattice = kagome.lattice

    n_sites = length(kagome.lattice.supercell.sites)
    @mylogmsg "Number of sites: $n_sites"
    for target_state_vec in target_state_vec_list
        if length(target_state_vec) != n_sites
            error("target_state length $(length(target_state_vec)) does not match number of sites ($n_sites)")
        end
    end

    if n_sites <= 64
        BR = UInt64
    elseif n_sites <= 128
        BR = UInt128
    elseif n_sites <= 256
        BR = UInt256
    elseif n_sites <= 512
        BR = UInt512
    elseif n_sites <= 1024
        BR = UInt1024
    else
        error("Too many sites to be reprented with unsigned integer")
    end

    hs, pauli = QuantumHamiltonian.Toolkit.spin_half_system(n_sites, BR)
    qns = Int[]
    target_states = BR[]
    for target_state_vec in target_state_vec_list
        target_state = compress(hs, target_state_vec)
        qn = get_quantum_number(hs, target_state)[1]
        push!(qns, qn)
        push!(target_states, target_state)
    end
    sort!(qns)
    unique!(qns)

    @mylogmsg "target states: $target_states"
    @mylogmsg "target quantum numbers: $qns"
    
    # sx = 0.5 * sum(pauli(i, :x) for i in 1:n_sites)
    # sy = 0.5 * sum(pauli(i, :y) for i in 1:n_sites)
    # sz = 0.5 * sum(pauli(i, :z) for i in 1:n_sites)
    # spin_squared = simplify(sx*sx + sy*sy + sz*sz)

    @mylogmsg "Generating Hamiltonian"
    jx, jy, jz = NullOperator(), NullOperator(), NullOperator()
    for hexa in kagome.motifs.hexagons
        n = length(hexa)
        @assert n == 6
        for i1 in 1:6, i2 in (i1+1):6
            i, j = first(hexa[i1]), first(hexa[i2])
            jx += pauli(i, :x) * pauli(j, :x)
            jy += pauli(i, :y) * pauli(j, :y)
            jz += pauli(i, :z) * pauli(j, :z)
        end
    end
    hamiltonian = simplify(Jx*jx + Jy*jy + Jz*jz) * 0.25

    @mylogmsg "Quantum numbers: $(quantum_number_sectors(hs))"

    for qn in qns
        Sz = qn / 2
        @mylogmsg "spin_z: $Sz"

        @mylogmsg "Creating Hilbert space sector"
        hss = HilbertSpaceSector(hs, qn)
        @mylogmsg "Representing the Hilbert space sector"
        hssr = represent_dict(hss)
        @mylogmsg "    dimension : $(dimension(hssr))"

        ssymbed = kagome.space_symmetry_embedding

        for ssic in get_irrep_components(ssymbed)
            if !isempty(tsym_irrep_indices) && (ssic.normal.irrep_index ∉ tsym_irrep_indices)
                continue
            elseif !isempty(psym_irrep_indices) && (ssic.rest.irrep_index ∉ psym_irrep_indices)
                continue
            elseif !isempty(psym_irrep_components) && (ssic.rest.irrep_component ∉ psym_irrep_components)
                continue
            end

            k = fractional_momentum(ssymbed, ssic.normal.irrep_index)
            rhssr = symmetry_reduce(hssr, ssic)

            valid_target_states = BR[]
            let
                compatible = false
                for target_state in target_states
                    target_index_parent = hssr.basis_lookup[target_state]
                    target_index_reduced = rhssr.basis_mapping_index[target_index_parent]
                    target_index_reduced <= 0 && continue
                    
                    target_state_string = string(target_state; base=2, pad=n_sites)

                    parameter_filename = Dict{Symbol, Any}(
                        :shape => shape_str,
                        :Jx=>format("{:.3f}", Jx),
                        :Jy=>format("{:.3f}", Jy),
                        :Jz=>format("{:.3f}", Jz),
                        :target=>target_state_string,
                        # :Sz=>format("{:.1f}", Sz),
                        :tii=>ssic.normal.irrep_index,
                        :pii=>ssic.rest.irrep_index,
                        :pic=>ssic.rest.irrep_component,
                    )
                    matrix_type = "dense"
                    output_filename = savename("overlap-$matrix_type", parameter_filename, "msgpack.xz")
                    output_filepath = datadir("kagome-xxz-overlap", "($n11,$n21)x($n12,$n22)", output_filename)
                    if ispath(output_filepath)
                        @mylogmsg "File $output_filepath exists."
                        if force
                            @mylogmsg "Overwriting."
                        else
                            @mylogmsg "Skipping."
                            continue
                        end
                    end
                    compatible = true
                    push!(valid_target_states, target_state)
                end
                !compatible && continue
            end

            @mylogmsg "  - trans symmetry irrep index : $(ssic.normal.irrep_index)"
            @mylogmsg "    momentum (in units of 2π)  : $(kagome.lattice.unitcell.reducedreciprocallatticevectors * k)"
            @mylogmsg "    little point symmetry      : $(symmetry(ssic.rest.symmetry).hermann_mauguin)"
            @mylogmsg "    point symmetry irrep index : $(ssic.rest.irrep_index)"
            @mylogmsg "    point symmetry irrep compo : $(ssic.rest.irrep_component)"
            hilbert_space_dimension = dimension(rhssr)
            @mylogmsg "    Hilbert space dimension    : $(hilbert_space_dimension)"

            hilbert_space_dimension == 0 && continue

            @assert hilbert_space_dimension <= max_dense
            matrix_type = "dense"
            # if hilbert_space_dimension > max_dense
            #     matrix_type = "sparse"
            # else
            #     matrix_type = "dense"
            # end
            # @mylogmsg "matrix type: $matrix_type"

            # if measure
            #     output_filename = savename("overlap-$matrix_type-measure", parameter_filename, "msgpack.xz")
            # else


            hamiltonian_rep = represent(rhssr, hamiltonian)
            # spin_squared_rep = represent(rhssr, spin_squared)

            @mylogmsg "# Computing eigenspectrum"
            # if matrix_type == "sparse"
            #     eigenvalues, eigenvectors = eigs(hamiltonian_rep; nev=nev, which=:SR)
            #     eigenvalues = real.(eigenvalues)
            # else  # if matrix_type == "dense"
            hamiltonian_dense = Matrix(hamiltonian_rep)
            # eigenvalues, eigenvectors = npl.eigh(hamiltonian_dense)
            eigenvalues, eigenvectors = eigen(Hermitian(hamiltonian_dense))
            # end

            for target_state in valid_target_states
                target_state_vec = extract(hs, target_state)
                target_index_parent = hssr.basis_lookup[target_state]
                target_state_string = string(target_state; base=2, pad=n_sites)
        
                target_index_reduced = rhssr.basis_mapping_index[target_index_parent]
                target_amplitude_reduced = rhssr.basis_mapping_amplitude[target_index_parent]

                if target_index_reduced <= 0
                    continue
                end

                parameter_filename = Dict{Symbol, Any}(
                    :shape => shape_str,
                    :Jx=>format("{:.3f}", Jx),
                    :Jy=>format("{:.3f}", Jy),
                    :Jz=>format("{:.3f}", Jz),
                    :target=>target_state_string,
                    # :Sz=>format("{:.1f}", Sz),
                    :tii=>ssic.normal.irrep_index,
                    :pii=>ssic.rest.irrep_index,
                    :pic=>ssic.rest.irrep_component,
                )
                output_filename = savename("overlap-$matrix_type", parameter_filename, "msgpack.xz")
                output_filepath = datadir("kagome-xxz-overlap", "($n11,$n21)x($n12,$n22)", output_filename)
                @mylogmsg "# Will save results to $output_filepath"
                if ispath(output_filepath)
                    @mylogmsg "File $output_filepath exists."
                    if force
                        @mylogmsg "Overwriting."
                    else
                        @mylogmsg "Skipping."
                        continue
                    end
                end
                        
                amplitudes = eigenvectors[target_index_reduced, :]

                save_data = OrderedDict(
                    "parameter" => OrderedDict(
                        "Jx" => Jx,
                        "Jy" => Jy,
                        "Jz" => Jz,
                        "shape" => [shape[1,1], shape[2,1], shape[1,2], shape[2,2]]
                    ),
                    "sector" => OrderedDict(
                        "Sz" => Sz,
                        "tsym_irrep_index" => ssic.normal.irrep_index,
                        "psym_irrep_index" => ssic.rest.irrep_index,
                        "psym_irrep_component" => ssic.rest.irrep_component,
                        "matrix_type" => matrix_type,
                        "dimension" => hilbert_space_dimension,
                    ),
                    "target_state" => target_state_vec.I,
                    "eigenvalue" => eigenvalues,
                    "overall_amplitude_real" => real(target_amplitude_reduced),
                    "overall_amplitude_imag" => imag(target_amplitude_reduced),
                    "amplitude_real" => real.(amplitudes),
                    "amplitude_imag" => imag.(amplitudes),
                    # "spin_two" => spin_two_list,
                    # "spin_four" => spin_four_list,
                )

                @mylogmsg "# Saving to $output_filepath"
                let output_directory = dirname(output_filepath)
                    if !isdir(output_directory)
                        mkpath(output_directory)
                    end
                end
                open(output_filepath, "w") do io
                    ioc = XzCompressorStream(io)
                    MsgPack.pack(ioc, save_data)
                    close(ioc)
                end
            end # for target_state
        end # for ssic
    end # for (qn,)
end



function parse_commandline()
    s = ArgParse.ArgParseSettings()
    ArgParse.@add_arg_table! s begin
        "shape"
            arg_type = String
            help = "shape of the lattice in the format (?,?)x(?,?)"
            required = true
        "--Jx"
            arg_type = Float64
            nargs = '+'
            default = [1.0]
        "--Jy"
            arg_type = Float64
            nargs = '+'
            default = [1.0]
        "--Jz"
            arg_type = Float64
            nargs = '+'
            default = [1.0]
        "--target"
            arg_type = String
            nargs = '+'
            help = "target state (zero based)"
        "--tii"
            arg_type = Int
            nargs = '*'
            help = "tii"
        "--pii"
            arg_type = Int
            nargs = '*'
            help = "pii"
        "--pic"
            arg_type = Int
            nargs = '*'
            help = "pic"
        "--max-dense"
            arg_type = Int
            default = 20000
            range_tester = x -> (x>0)
            help = "maximum hilbert space dimension to solve with dense matrix"
        "--nev"
            arg_type = Int
            default = 500
            range_tester = x -> (x>0)
            help = "number of eigenvalues to compute when using sparse"
        "--debug", "-d"
            help = "debug"
            action = :store_true
        "--measure", "-m"
            help = "measure"
            action = :store_true
        "--force", "-f"
            help = "force run (overwrite)"
            action = :store_true
    end
    return ArgParse.parse_args(s)
end

function parse_shape(shape_str::AbstractString)
    shape_pattern = r"\(\s*([-+]?\d+)\s*,\s*([-+]?\d+)\s*\)x\(\s*([-+]?\d+)\s*,\s*([-+]?\d+)\s*\)"
    m = match(shape_pattern, shape_str)
    if isnothing(m)
        throw(ArgumentError("shape should be in format (n11,n12)x(n21,n22)"))
    end
    n11, n12, n21, n22 = [parse(Int, x) for x in m.captures]
    return [n11 n21;
            n12 n22]
end


function main()

    parsed_args = parse_commandline()

    if parsed_args["debug"]
        logger = ConsoleLogger(stdout, Logging.Debug; meta_formatter=my_metafmt)
        global_logger(logger)
    else
        logger = ConsoleLogger(stdout, Logging.Info; meta_formatter=my_metafmt)
        global_logger(logger)
    end

    shape = parse_shape(parsed_args["shape"])

    Jxs = parsed_args["Jx"]
    Jys = parsed_args["Jy"]
    Jzs = parsed_args["Jz"]

    # Sz_list = parsed_args["Sz"]
    tii_list = parsed_args["tii"]
    pii_list = parsed_args["pii"]
    pic_list = parsed_args["pic"]

    max_dense = parsed_args["max-dense"]
    nev = parsed_args["nev"]
    measure = parsed_args["measure"]
    force = parsed_args["force"]

    target_state_vec_list = [
        CartesianIndex([parse(Int, c) + 1 for c in reverse(args_target)]...)
        for args_target in parsed_args["target"]
    ]

    for (Jx, Jy, Jz) in Iterators.product(Jxs, Jys, Jzs)
        compute_kagome_bfg(
            Jx, Jy, Jz,
            shape,
            target_state_vec_list,
            tii_list, pii_list, pic_list,
            max_dense,
            # nev,
            # measure,
            force,
        )
    end
end


main()
