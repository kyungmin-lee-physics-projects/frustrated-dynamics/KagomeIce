using Formatting
using LinearAlgebra
using Arpack

using Plots


using LatticeTools
using QuantumHamiltonian

using KagomeIce



function main()
    shape = [5 0; 0 5]
    kagome = make_kagome_lattice(shape)
    n_sites = numsite(kagome.lattice.supercell)

    BR = if n_sites <= 64
        UInt64
    elseif n_sites <= 128
        UInt128
    else
        @assert false
    end

    (hs, spinop) = QuantumHamiltonian.Toolkit.spin_system(n_sites, 1//2, BR)

    hamiltonian = NullOperator()

    for hexagon in kagome.motifs.hexagons
        @assert length(hexagon) == 6

        hamiltonian += prod(spinop(i, opsym) for ((i, R), opsym) in zip(hexagon, [:+,:-,:+,:-,:+,:-]))
        hamiltonian += prod(spinop(i, opsym) for ((i, R), opsym) in zip(hexagon, [:-,:+,:-,:+,:-,:+]))
    end
    hamiltonian = simplify(hamiltonian)

    ice_basis_list = generate_basis(shape, BR)
    hsr = represent_dict(hs, ice_basis_list)
    println("total dim = $(dimension(hsr))")

    ssym = TranslationSymmetry(kagome.lattice) ⋊ PointSymmetryDatabase.find2d("6mm")
    ssymbed = embed(kagome.lattice, ssym)
    total_dim = 0
    all_eigenvalues = Float64[]
    for ssic in get_irrep_components(ssymbed)
        rhsr = symmetry_reduce(hsr, ssic)
        dim = dimension(rhsr)
        dim == 0 && continue
        total_dim += dim
        hamiltonian_rep = represent(rhsr ,hamiltonian)
        hamiltonian_matrix = Matrix(hamiltonian_rep)
        @assert maximum(abs.(hamiltonian_matrix - transpose(conj(hamiltonian_matrix)))) < Base.rtoldefault(Float64)
        eigenvalues = eigvals!(Hermitian(hamiltonian_matrix))
        append!(all_eigenvalues, eigenvalues)
    end
    sort!(all_eigenvalues)
    #scatter(all_eigenvalues, markersize=1)
    histogram(all_eigenvalues, bins=600, density=true)
end

main()
