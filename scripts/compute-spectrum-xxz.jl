using DrWatson
quickactivate(joinpath(@__DIR__, ".."))

using Logging
using Random
using LinearAlgebra
using SparseArrays
using Printf
using DataStructures

using BitIntegers
using Formatting

import ArgParse
using Arpack

using LatticeTools
using QuantumHamiltonian
using KagomeIce

using CodecXz
using MsgPack

using PyCall
npl = pyimport("numpy.linalg")


function compute_kagome_bfg(
    Jperp::Real, Jz::Real,
    shape::AbstractMatrix{<:Integer},

    Sz_list::AbstractVector{<:Real},
    z2_list::AbstractVector{<:Integer},
    tii_list::AbstractVector{<:Integer},
    pii_list::AbstractVector{<:Integer},
    pic_list::AbstractVector{<:Integer},

    max_dense::Integer, nev::Integer,
    force::Bool=true,
    dryrun::Bool=false,
)
    @mylogmsg "lattice shape: $shape"
    @mylogmsg "Jperp: $Jperp"
    @mylogmsg "Jz: $Jz"

    n11 = shape[1,1]
    n12 = shape[1,2]
    n21 = shape[2,1]
    #n12 = shape[2,1]
    #n21 = shape[1,2]
    n22 = shape[2,2]
    shape_str = "($n11,$n21)x($n12,$n22)"

    kagome = KagomeIce.make_kagome_lattice(shape)
    lattice = kagome.lattice
    ssymbed = kagome.space_symmetry_embedding

    n_sites = length(kagome.lattice.supercell.sites)
    @mylogmsg "Number of sites: $n_sites"

    if n_sites <= 64
        BR = UInt64
    elseif n_sites <= 128
        BR = UInt128
    elseif n_sites <= 256
        BR = UInt256
    elseif n_sites <= 512
        BR = UInt512
    elseif n_sites <= 1024
        BR = UInt1024
    else
        error("Too many sites to be reprented with unsigned integer")
    end

    hs, pauli = QuantumHamiltonian.Toolkit.spin_half_system(n_sites, BR)

    @mylogmsg "Generating Hamiltonian"
    jx, jy, jz = NullOperator(), NullOperator(), NullOperator()
    for hexa in kagome.motifs.hexagons
        n = length(hexa)
        @assert n == 6
        for i1 in 1:6, i2 in (i1+1):6
            i, j = first(hexa[i1]), first(hexa[i2])
            jx += pauli(i, :x) * pauli(j, :x)
            jy += pauli(i, :y) * pauli(j, :y)
            jz += pauli(i, :z) * pauli(j, :z)
        end
    end
    hamiltonian = simplify((jx + jy)*Jperp + jz*Jz) * 0.25

    @mylogmsg "Quantum numbers: $(quantum_number_sectors(hs))"
    for qn in [0]
        Sz = qn / 2
        if !isempty(Sz_list) && (Sz ∉ Sz_list)
            continue
        end
        @mylogmsg "spin_z: $Sz"
        hss = HilbertSpaceSector(hs, qn)

        @mylogmsg "Representing Hilbert space sector"
        hssr = represent_dict(hss)
        @mylogmsg "dimension: $(dimension(hssr))"

        z2signs = (qn == 0) ? [1, -1] : [0]
        for z2sign in z2signs
            if z2sign == 0
                z2_symops_and_amplitudes = [(GlobalBitFlip(false), 1)]
            else
                @assert abs(z2sign) == 1
                z2_symops_and_amplitudes = [(GlobalBitFlip(false), 1), (GlobalBitFlip(true), z2sign)]
            end
            if !isempty(z2_list) && (z2sign ∉ z2_list)
                continue
            end

            for tsic in get_irrep_components(ssymbed.normal)
                if !isempty(tii_list) && (tsic.irrep_index ∉ tii_list)
                    continue
                end
                t_symops_and_amplitudes = collect(get_irrep_iterator(tsic))
                k = fractional_momentum(ssymbed, tsic.irrep_index)
                psym_little = little_symmetry(tsic, ssymbed.rest)
                for psic in get_irrep_components(psym_little)
                    if !isempty(pii_list) && (psic.irrep_index ∉ pii_list)
                        continue
                    elseif !isempty(pic_list) && (psic.irrep_component ∉ pic_list)
                        continue
                    end
                    p_symops_and_amplitudes = collect(get_irrep_iterator(psic))

                    symops_and_amplitudes = make_product_irrep(z2_symops_and_amplitudes, t_symops_and_amplitudes, p_symops_and_amplitudes)

                    rhssr = symmetry_reduce(hssr, symops_and_amplitudes)

                    hilbert_space_dimension = dimension(rhssr)
                    hilbert_space_dimension == 0 && continue

                    parameter_filename = Dict{Symbol, Any}(
                        :shape => shape_str,
                        :Jperp=>format("{:.6f}", Jperp),
                        :Jz=>format("{:.6f}", Jz),
                        :Sz=>format("{:.1f}", Sz),
                        :z2=>z2sign,
                        :tii=>tsic.irrep_index,
                        :pii=>psic.irrep_index,
                        :pic=>psic.irrep_component,
                    )

                    if hilbert_space_dimension > max_dense
                        matrix_type = "sparse"
                    else
                        matrix_type = "dense"
                    end

                    output_filename = savename("spectrum-$matrix_type", parameter_filename, "msgpack.xz")
                    output_filepath = datadir("kagome-xxz", "($n11,$n21)x($n12,$n22)", output_filename)

                    @mylogmsg "  - Sz                         : $(Sz)"
                    @mylogmsg "    z2 spin flip               : $(z2sign)"
                    @mylogmsg "    trans symmetry irrep index : $(tsic.irrep_index)"
                    @mylogmsg "    momentum (in units of 2π)  : $(kagome.lattice.unitcell.reducedreciprocallatticevectors * k)"
                    @mylogmsg "    little point symmetry      : $(symmetry(psic.symmetry).hermann_mauguin)"
                    @mylogmsg "    point symmetry irrep index : $(psic.irrep_index)"
                    @mylogmsg "    point symmetry irrep compo : $(psic.irrep_component)"
                    @mylogmsg "    Hilbert space dimension    : $(hilbert_space_dimension)"
                    @mylogmsg "    matrix type                : $(matrix_type)"

                    if ispath(output_filepath)
                        @mylogmsg "File $output_filepath exists."
                        if force
                            @mylogmsg "Overwriting."
                        else
                            @mylogmsg "Skipping."
                            continue
                        end
                    end
                    @mylogmsg "# Will save results to $output_filepath"

                    dryrun && continue

                    hamiltonian_rep = represent(rhssr, hamiltonian)
                    if matrix_type == "sparse"
                        @mylogmsg "Diagonalizing sparse Hamiltonian"
                        eigenvalues, eigenvectors = eigs(hamiltonian_rep; nev=nev, which=:SR)
                        eigenvalues = real.(eigenvalues)
                    else  # if matrix_type == "dense"
                        @mylogmsg "Creating dense Hamiltonian matrix"
                        hamiltonian_dense = Matrix(hamiltonian_rep)
                        @mylogmsg "Diagonalizing dense Hamiltonian matrix"
                        if hilbert_space_dimension < 32767 # https://github.com/numpy/numpy/issues/13956
                            # eigenvalues, eigenvectors = npl.eigh(hamiltonian_dense)
                            eigenvalues = npl.eigvalsh(hamiltonian_dense)
                        else
                            # eigenvalues, eigenvectors = eigen(Hermitian(hamiltonian_dense))
                            eigenvalues = eigvals(Hermitian(hamiltonian_dense))
                        end
                    end

                    save_data = OrderedDict(
                        "parameter" => OrderedDict(
                            "Jperp" => Jperp,
                            "Jz" => Jz,
                            "shape" => [shape[1,1], shape[2,1], shape[1,2], shape[2,2]]
                        ),
                        "sector" => OrderedDict(
                            "Sz" => Sz,
                            "spin_flip" => z2sign,
                            "tsym_irrep_index" => tsic.irrep_index,
                            "psym_irrep_index" => psic.irrep_index,
                            "psym_irrep_component" => psic.irrep_component,
                            "dimension" => hilbert_space_dimension,
                        ),
                        "eigenvalue" => eigenvalues,
                    )

                    @mylogmsg "# Saving to $output_filepath"
                    let output_directory = dirname(output_filepath)
                        if !isdir(output_directory)
                            mkpath(output_directory)
                        end
                    end
                    open(output_filepath, "w") do io
                        ioc = XzCompressorStream(io)
                        MsgPack.pack(ioc, save_data)
                        close(ioc)
                    end
                end # psic
            end # tsic
        end # z2sign

    end # for (qn,)
end


function parse_commandline()
    s = ArgParse.ArgParseSettings()
    ArgParse.@add_arg_table! s begin
        "shape"
            arg_type = String
            help = "shape of the lattice in the format (?,?)x(?,?)"
            required = true
        "--Jperp"
            arg_type = Float64
            required = true
        "--Jz"
            arg_type = Float64
            default = 1.0
        "--Sz"
            arg_type = Float64
            nargs = '*'
            help = "values of Sz to consider"
        "--z2"
            arg_type = Int
            nargs = '*'
            help = "z2"
        "--tii"
            arg_type = Int
            nargs = '*'
            help = "tii"
        "--pii"
            arg_type = Int
            nargs = '*'
            help = "pii"
        "--pic"
            arg_type = Int
            nargs = '*'
            help = "pic"

        "--max-dense"
            arg_type = Int
            default = 20000
            range_tester = x -> (x>0)
            help = "maximum hilbert space dimension to solve with dense matrix"
        "--nev"
            arg_type = Int
            default = 500
            range_tester = x -> (x>0)
            help = "number of eigenvalues to compute when using sparse"
        "--debug", "-d"
            help = "debug"
            action = :store_true
        "--measure", "-m"
            help = "measure"
            action = :store_true
        "--force", "-f"
            help = "force run (overwrite)"
            action = :store_true
        "--dryrun", "-n"
            action = :store_true
    end
    return ArgParse.parse_args(s)
end

function parse_shape(shape_str::AbstractString)
    shape_pattern = r"\(\s*([-+]?\d+)\s*,\s*([-+]?\d+)\s*\)x\(\s*([-+]?\d+)\s*,\s*([-+]?\d+)\s*\)"
    m = match(shape_pattern, shape_str)
    if isnothing(m)
        throw(ArgumentError("shape should be in format (n11,n12)x(n21,n22)"))
    end
    n1a, n1b, n2a, n2b = [parse(Int, x) for x in m.captures]
    return [n1a n2a;
            n1b n2b]
end


function main()

    parsed_args = parse_commandline()

    if parsed_args["debug"]
        logger = ConsoleLogger(stdout, Logging.Debug; meta_formatter=my_metafmt)
        global_logger(logger)
    else
        logger = ConsoleLogger(stdout, Logging.Info; meta_formatter=my_metafmt)
        global_logger(logger)
    end

    shape = parse_shape(parsed_args["shape"])

    Jperp = parsed_args["Jperp"]
    Jz = parsed_args["Jz"]


    Sz_list = parsed_args["Sz"]
    z2_list = parsed_args["z2"]
    tii_list = parsed_args["tii"]
    pii_list = parsed_args["pii"]
    pic_list = parsed_args["pic"]

    max_dense = parsed_args["max-dense"]
    nev = parsed_args["nev"]
    # measure = parsed_args["measure"]
    force = parsed_args["force"]
    dryrun = parsed_args["dryrun"]

    compute_kagome_bfg(
        Jperp, Jz,
        shape,
        Sz_list,
        z2_list,
        tii_list,
        pii_list,
        pic_list,
        max_dense,
        nev,
        force,
        dryrun,
    )
end


main()
