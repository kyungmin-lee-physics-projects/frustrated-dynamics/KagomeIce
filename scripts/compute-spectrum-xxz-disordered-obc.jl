# PBC Jz, OBC J. Cut off bonds that go across
using DrWatson
quickactivate(joinpath(@__DIR__, ".."))

using Logging
using Random
using LinearAlgebra
using SparseArrays
using Printf
using DataStructures

using BitIntegers
using Formatting

import ArgParse
using Arpack
using KrylovKit

using LatticeTools
using QuantumHamiltonian
using KagomeIce

using CodecXz
using MsgPack
using JLD2

using PyCall
npl = pyimport("numpy.linalg")


function compute_kagome_bfg(
    Jperp::Tuple{<:Real, <:Real}, Jz::Real,
    shape::AbstractMatrix{<:Integer},
    Sz_list::AbstractVector{<:Real},
    spinflip_list::AbstractVector{<:Integer},
    max_dense::Integer, nev::Integer,
    seed::Integer,
    force::Bool=true,
)
    Jperpmin, Jperpmax = Jperp

    @mylogmsg "lattice shape: $shape"
    @mylogmsg "Jperp: $Jperp"
    @mylogmsg "Jz: $Jz"

    n1a = shape[1,1]
    n1b = shape[2,1]
    n2a = shape[1,2]
    n2b = shape[2,2]
    shape_str = "($n1a,$n1b)x($n2a,$n2b)"

    kagome = KagomeIce.make_kagome_lattice(shape)
    lattice = kagome.lattice

    n_sites = length(kagome.lattice.supercell.sites)
    @mylogmsg "Number of sites: $n_sites"

    if n_sites <= 64
        BR = UInt64
    elseif n_sites <= 128
        BR = UInt128
    elseif n_sites <= 256
        BR = UInt256
    elseif n_sites <= 512
        BR = UInt512
    elseif n_sites <= 1024
        BR = UInt1024
    else
        error("Too many sites to be reprented with unsigned integer")
    end

    hs, pauli = QuantumHamiltonian.Toolkit.spin_half_system(n_sites, BR)

    rng = Random.MersenneTwister(seed)
    @mylogmsg "Generating Hamiltonian"
    jx, jy, jz = NullOperator(), NullOperator(), NullOperator()
    for hexa in kagome.motifs.hexagons
        n = length(hexa)
        @assert n == 6
        for i1 in 1:6, i2 in (i1+1):6
            i, Ri = hexa[i1]
            j, Rj = hexa[i2]
            jz += pauli(i, :z) * pauli(j, :z) * Jz

            if Ri == Rj
                ampl = rand(rng, Float64) * (Jperpmax - Jperpmin) + Jperpmin
                jx += pauli(i, :x) * pauli(j, :x) * ampl
                jy += pauli(i, :y) * pauli(j, :y) * ampl
            end
        end
    end
    hamiltonian = simplify(jx + jy + jz) * 0.25

    @mylogmsg "Quantum numbers: $(quantum_number_sectors(hs))"

    for (qn,) in quantum_number_sectors(hs)
        Sz = qn / 2
        if !isempty(Sz_list) && (Sz ∉ Sz_list)
            continue
        end

        @mylogmsg "Sz: $Sz"
        @mylogmsg "Creating Hilbert space sector"
        hss = HilbertSpaceSector(hs, qn)
        hssr = represent_dict(hss)
        @mylogmsg "Unreduced Hilbert space dimension: $(dimension(hssr))"



        if qn == 0
            ice_basis_list = KagomeIce.BalentsFisherGirvin.generate_basis(kagome)
            ice_basis_index_list = Int[]
            for bvec in ice_basis_list
                ivec = get(hssr.basis_lookup, bvec, -1)
                ivec > 0 && push!(ice_basis_index_list, ivec)
            end
        end

        spinflips = (qn == 0) ? [1, -1] : [0]
        for spinflip in spinflips
            if !isempty(spinflip_list) && (spinflip ∉ spinflip_list)
                continue
            end

            @mylogmsg "spinflip: $spinflip"
            if spinflip == 0
                symops_and_amplitudes = [(GlobalBitFlip(false), 1.0)]
            else
                @assert abs(spinflip) == 1
                symops_and_amplitudes = [(GlobalBitFlip(false), 1.0), (GlobalBitFlip(true), float(spinflip))]
            end

            rhssr = symmetry_reduce(hssr, symops_and_amplitudes)
            hilbert_space_dimension = dimension(rhssr)
            hilbert_space_dimension == 0 && continue
            @mylogmsg "Hilbert space dimension: $(hilbert_space_dimension)"

            if hilbert_space_dimension > max_dense
                matrix_type = "sparse"
            else
                matrix_type = "dense"
            end            
            @mylogmsg "matrix type: $matrix_type"

            parameter_filename = Dict{Symbol, Any}(
                :shape => shape_str,
                :Jperp=>format("({:.3f},{:.3f})", Jperpmin, Jperpmax),
                :Jz=>format("{:.3f}", Jz),
                :Sz=>format("{:.1f}", Sz),
                :spinflip=>spinflip,
                :seed=>seed,
            )
            output_filename = savename("spectrum-$matrix_type", parameter_filename, "msgpack.xz")
            output_filepath = datadir("kagome-xxz-disordered-obc", "($n1a,$n1b)x($n2a,$n2b)", output_filename)

            if ispath(output_filepath)
                if force
                    @mylogmsg "File $output_filepath exists. Overwriting."
                else
                    @mylogmsg "File $output_filepath exists. Skipping."
                    continue
                end
            end
            @mylogmsg "Will save results to $output_filepath."

            hamiltonian_rep = represent(rhssr, hamiltonian)

            if matrix_type == "sparse"
                @mylogmsg "Diagonalizing sparse Hamiltonian"
                
                initial_state = zeros(ComplexF64, hilbert_space_dimension)
                for ivec_parent in ice_basis_index_list
                    ivec_reduced = rhssr.basis_mapping_index[ivec_parent]
                    ivec_reduced <= 0 && continue
                    initial_state[ivec_reduced] = randn(ComplexF64)
                end
                normalize!(initial_state)

                # eigenvalues, eigenvectors = eigs(hamiltonian_rep; nev=nev, which=:SR)
                eigenvalues, eigenvectors, info = let
                    H = x -> hamiltonian_rep * x
                    x₀ = initial_state
                    eigsolve(
                        H, x₀, nev, :SR;
                        ishermitian=true,
                        verbosity=2,
                        maxiter=5000,
                        krylovdim=nev*2+10,
                        tol=Base.rtoldefault(Float64),
                    )
                end
                if info.converged == 0
                    @warn "convergence problem: $info"
                    # @save "dump-$(getpid())-$(Dates.datetime2epochms(now())).jld2" shape Jperpmin Jperpmax Jz Sz spinflip seed eigenvalues eigenvectors info
                    # continue
                end
                eigenvalues = real.(eigenvalues)
                info = OrderedDict(
                    "converged" => info.converged,
                    # "residual" => info.residual,
                    "normres" => info.normres,
                    "numops" => info.numops,
                    "numiter" => info.numiter,
                )
            else  # if matrix_type == "dense"
                @mylogmsg "Creating dense Hamiltonian matrix"
                hamiltonian_dense = Matrix(hamiltonian_rep)
                @mylogmsg "Diagonalizing dense Hamiltonian matrix"
                if hilbert_space_dimension < 32768
                    eigenvalues = npl.eigvalsh(hamiltonian_dense)
                else
                    eigenvalues = eigvals!(Hermitian(hamiltonian_dense))
                end
                info = OrderedDict()
            end

            save_data = OrderedDict(
                "parameter" => OrderedDict(
                    "Jperp" => Jperp,
                    "Jz" => Jz,
                    "shape" => [shape[1,1], shape[2,1], shape[1,2], shape[2,2]]
                ),
                "sector" => OrderedDict(
                    "Sz" => Sz,
                    "spinflip" => spinflip,
                    "dimension" => hilbert_space_dimension,
                    "matrix_type" => matrix_type,
                ),
                "eigenvalue" => eigenvalues,
                "info" => info,
            )

            @mylogmsg "# Saving to $output_filepath"
            let output_directory = dirname(output_filepath)
                if !isdir(output_directory)
                    mkpath(output_directory)
                end
            end
            open(output_filepath, "w") do io
                ioc = XzCompressorStream(io)
                MsgPack.pack(ioc, save_data)
                close(ioc)
            end
        end # for spinflip
    end # for (qn,)
end


function parse_commandline()
    s = ArgParse.ArgParseSettings()
    ArgParse.@add_arg_table! s begin
        "shape"
            arg_type = String
            help = "shape of the lattice in the format (?,?)x(?,?)"
            required = true
        "--Jperp"
            arg_type = Float64
            nargs = 2
            required = true
        "--Jz"
            arg_type = Float64
            default = 1.0
        "--Sz"
            arg_type = Float64
            nargs = '*'
            help = "values of Sz to consider"
        "--spinflip"
            arg_type = Int
            nargs = '*'
            help = "spinflip"
        "--max-dense"
            arg_type = Int
            default = 32768
            range_tester = x -> (x>0)
            help = "maximum hilbert space dimension to solve with dense matrix"
        "--nev"
            arg_type = Int
            default = 500
            range_tester = x -> (x>0)
            help = "number of eigenvalues to compute when using sparse"
        "--seed"
            arg_type = Int
            # default = 1
            required = true
            help = "random seed"
        "--debug", "-d"
            help = "debug"
            action = :store_true
        "--measure", "-m"
            help = "measure"
            action = :store_true
        "--force", "-f"
            help = "force run (overwrite)"
            action = :store_true
    end
    return ArgParse.parse_args(s)
end

function parse_shape(shape_str::AbstractString)
    shape_pattern = r"\(\s*([-+]?\d+)\s*,\s*([-+]?\d+)\s*\)x\(\s*([-+]?\d+)\s*,\s*([-+]?\d+)\s*\)"
    m = match(shape_pattern, shape_str)
    if isnothing(m)
        throw(ArgumentError("shape should be in format (n1a,n1b)x(n2a,n2b)"))
    end
    n1a, n1b, n2a, n2b = [parse(Int, x) for x in m.captures]
    return [n1a n2a;
            n1b n2b]
end


function main()

    parsed_args = parse_commandline()

    if parsed_args["debug"]
        logger = ConsoleLogger(stdout, Logging.Debug; meta_formatter=my_metafmt)
        global_logger(logger)
    else
        logger = ConsoleLogger(stdout, Logging.Info; meta_formatter=my_metafmt)
        global_logger(logger)
    end

    shape = parse_shape(parsed_args["shape"])

    Jperp = parsed_args["Jperp"]
    Jz = parsed_args["Jz"]

    Sz_list = parsed_args["Sz"]
    spinflip_list = parsed_args["spinflip"]
    # tii_list = parsed_args["tii"]
    # pii_list = parsed_args["pii"]
    # pic_list = parsed_args["pic"]

    max_dense = parsed_args["max-dense"]
    nev = parsed_args["nev"]
    seed = parsed_args["seed"]
    # measure = parsed_args["measure"]
    force = parsed_args["force"]

    compute_kagome_bfg(
        tuple(Jperp...), Jz,
        shape,
        Sz_list,
        spinflip_list,
        max_dense,
        nev,
        # measure,
        seed,
        force,
    )
end


main()
