using DrWatson
quickactivate(joinpath(@__DIR__, ".."))

using Glob
using ArgParse
# using SparseArrays
using LinearAlgebra
using DataStructures

using Random
using Logging
using Printf
using Formatting
using ArgParse

using CodecXz
using MsgPack
using BitIntegers

using Formatting

using LightGraphs
using PyCall
global const npl = pyimport("numpy.linalg")

using PyPlot
using GraphPlot
using Compose, Cairo

using LatticeTools
using QuantumHamiltonian
using KagomeIce

function get_spacings(values; digits::Integer=8, nondegenerate::Bool=false)
    values2 = round.(sort(values); digits=digits)
    for (i, x) in enumerate(values2)
        if iszero(x)
            values2[i] = zero(x)
        end
    end
    nondegenerate && unique!(values2)
    length(values2) < 2 && return Float64[]
    return values2[2:end] - values2[1:end-1]
end


function get_ratios(values; digits::Integer=8, nondegenerate::Bool=false)
    spacings = get_spacings(values, digits=digits, nondegenerate=nondegenerate)
    n = length(spacings)
    n <= 3 && return Float64[]

    m = n
    while (m > 1) && iszero(spacings[m])
        m -= 1
    end
    m <= 1 && return Float64[]

    s_prev = spacings[m] # nonzero
    ratios = Float64[]
    while m > 1
        m -= 1
        s = spacings[m]
        r = min(s, s_prev) / max(s, s_prev)
        if !iszero(s)
            s_prev = s
        end
        push!(ratios, r)
    end
    @show spacings
    @show ratios
    return ratios
end




function pdf_general(beta::Real)
    function pdf(x::Real)
        return (27.0/4) * (x+x^2)^(beta) / (1+x+x^2)^(1+3*beta/2)
    end
    function pdf(x::AbstractArray{<:Real})
        return (27.0/4) .* (x + x.^2).^(beta) ./ (1 + x + x .^ 2)^(1 + 3 .* beta./2)
    end
    return pdf
end

pdf_goe = pdf_general(1)
pdf_poisson(x::Real) = 2 / (1+x)^2
pdf_poisson(x::AbstractArray{<:Real}) = 2 ./ (1+x).^2

function plot_level_statistics(shape::AbstractMatrix{<:Integer}; digits::Integer=8, nondegenerate::Bool=false)
    xs = 0:0.01:1
    ys_goe = pdf_goe.(xs)
    ys_poi = pdf_poisson.(xs)

    shape_str = "($(shape[1,1]),$(shape[2,1]))x($(shape[1,2]),$(shape[2,2]))"
    data_directory = datadir("bfgice", shape_str)
    data_filepaths = Glob.glob("*.xz", data_directory)

    eigenvalues = []
    for filepath in data_filepaths
        open(filepath, "r") do io
            ioc = XzDecompressorStream(io)
            data = MsgPack.unpack(ioc)
            ev = hcat(data["eigenvalue"]...)
            push!(eigenvalues, ev)
        end
    end

    sector_count = length(eigenvalues[1])

    plots_directory = plotsdir("bfgice", shape_str)
    if !isdir(plots_directory)
        mkpath(plots_directory)
    end

    function plot_ratios(select_ratios::AbstractVector{<:Real}, fig)
        fig.clf()
        ax = fig.gca()

        ax.hist(select_ratios, bins=64, density=true, range=[0, 1], alpha=0.8, color="xkcd:grey")
        ax.plot(xs, ys_poi, label="Poisson", linestyle="--", linewidth=2, alpha=1, color="#4063D8")
        ax.plot(xs, ys_goe, label="GOE", linestyle=":", linewidth=2, alpha=1, color="#CB3C33")

        ax.set_xlim(0, 1)
        ax.set_ylim(0, 2.2)
        ax.set_yticks([0, 0.5, 1, 1.5, 2])

        ax.set_xlabel(raw"$\tilde{r}$", fontsize=12)
        ax.set_ylabel(raw"$P(\tilde{r})$", fontsize=12)
    end

    fig = PyPlot.figure(figsize=(4, 3.5))

    fnameprefix = nondegenerate ? "level-statistics-nondegenerate" : "level-statistics"
    for isector in 1:sector_count
        select_eigenvalues = [ev[isector] for ev in eigenvalues]
        select_ratios = vcat(get_ratios.(select_eigenvalues; digits=digits, nondegenerate=nondegenerate)...)
        isempty(select_ratios) && continue
        plot_ratios(select_ratios, fig)
        fig.savefig(joinpath(plots_directory, "$fnameprefix-sector_$isector.png"), dpi=300, bbox_inches="tight")
        fig.savefig(joinpath(plots_directory, "$fnameprefix-sector_$isector.pdf"), dpi=300, bbox_inches="tight")
    end

    let
        select_eigenvalues = [vcat(ev...) for ev in eigenvalues]
        select_ratios = vcat(get_ratios.(select_eigenvalues; digits=digits, nondegenerate=nondegenerate)...)
        plot_ratios(select_ratios, fig)
        fig.gca().legend(loc=1)
        fig.savefig(joinpath(plots_directory, "$fnameprefix-complete.png"), dpi=300, bbox_inches="tight")
        fig.savefig(joinpath(plots_directory, "$fnameprefix-complete.pdf"), dpi=300, bbox_inches="tight")
    end

    let
        select_eigenvalues = [vcat(ev[1:2:end]...) for ev in eigenvalues]
        select_ratios = vcat(get_ratios.(select_eigenvalues; digits=digits, nondegenerate=nondegenerate)...)
        plot_ratios(select_ratios, fig)
        fig.gca().legend(loc=1)
        fig.savefig(joinpath(plots_directory, "$fnameprefix-z2_symmetric.png"), dpi=300, bbox_inches="tight")
        fig.savefig(joinpath(plots_directory, "$fnameprefix-z2_symmetric.pdf"), dpi=300, bbox_inches="tight")
    end

    let
        select_eigenvalues = [vcat(ev[2:2:end]...) for ev in eigenvalues]
        select_ratios = vcat(get_ratios.(select_eigenvalues; digits=digits, nondegenerate=nondegenerate)...)
        plot_ratios(select_ratios, fig)
        fig.gca().legend(loc=1)
        fig.savefig(joinpath(plots_directory, "$fnameprefix-z2_antisymmetric.png"), dpi=300, bbox_inches="tight")
        fig.savefig(joinpath(plots_directory, "$fnameprefix-z2_antisymmetric.pdf"), dpi=300, bbox_inches="tight")
    end
end


function parse_commandline()
    s = ArgParseSettings()
    @add_arg_table! s begin
        "shape"
            arg_type = String
            required = true
        "--nondegenerate"
            action = :store_true
        "--digits"
            arg_type = Int
            required = false
            default = 8
    end
    return parse_args(s)
end

function main()
    args = parse_commandline()
    shape = KagomeIce.parse_shape(args["shape"])
    plot_level_statistics(shape; digits=args["digits"], nondegenerate=args["nondegenerate"])
end

main()
