using DrWatson
quickactivate(joinpath(@__DIR__, ".."))

using Logging
using Random
using LinearAlgebra
using SparseArrays
using Printf
using DataStructures

using BitIntegers
using Formatting

using Arpack
using StatsBase

using LatticeTools
using QuantumHamiltonian
using KagomeIce

# using SimpleLinear
using IterativeSolvers

using CodecXz
using MsgPack

# using PyCall
# npl = pyimport("numpy.linalg")

using LinearMaps
using LinearMapsAA
using SuperLU
using KrylovKit

import ArgParse

function string_error(value::T, error::T) where {T<:Number}
    if abs(error) <= eps(T)
        return "$(value) ± 0"
    else
        error_rounded = round(error; sigdigits=1)
        digits = ceil(Int, -log10(error_rounded))
        value_rounded = round(value, digits=digits)
        return "$(value_rounded) ± $(error_rounded)"
    end
end

# H⋅U = E⋅U
# U†⋅H⋅U = e
# V†⋅H⋅V = h = v⋅e⋅v† = v⋅U†⋅H⋅U⋅v†
function reeigen(op::Hermitian{T, <:AbstractMatrix{T}}, basis::AbstractMatrix{S}) where {T, S}
    @boundscheck if LinearAlgebra.checksquare(op) != size(basis, 1)
        throw(DimensionMismatch("op has dimensions $(size(op)), and basis has dimensions $(size(basis))"))
    end
    op_prime = adjoint(basis) * (op.data * basis)
    eigen_prime = eigen!(Hermitian(op_prime))
    return eigen_prime.values
    # return Eigen(eigen_prime.values, basis * adjoint(eigen_prime.vectors))
end

# function Base.:(*)(A::AbstractOperatorRepresentation{S}, B::AbstractArray{T}) where {S, T}
#     size(A, 2) == size(B, 1) || throw(DimensionMismatch("A has size $(size(A)) and B has size $(size(B))"))
#     U = promote_type(S, T)
#     iter_size = size(B)[2:end]
#     C = zeros(U, (size(A, 1), iter_size...))
#     for i3 in CartesianIndices(iter_size)
#         apply!(view(C, :, i3...), A, view(B, :, i3...))
#     end
#     return C
# end

function Base.:(*)(A::AbstractOperatorRepresentation{S}, B::AbstractMatrix{T}) where {S, T}
    size(A, 2) == size(B, 1) || throw(DimensionMismatch("A has size $(size(A)) and B has size $(size(B))"))
    U = promote_type(S, T)
    C = zeros(U, (size(A, 1), size(B, 2)))
    Threads.@threads for i3 in 1:size(B, 2)
        apply!(view(C, :, i3), A, view(B, :, i3))
    end
    return C
end

function estimate_density(
    rng::AbstractRNG,
    m::AbstractOperatorRepresentation{T};
    nsample::Integer=min(10, size(m, 2)),
    tol::Real=Base.rtoldefault(real(T))
) where {T}
    dim = size(m, 2)
    vals = Dict{Int, T}()
    function column_density(col::Integer)
        return sum(x -> 1, get_column_iterator(m, col)) / dim
    end

    if dim < 10
        md = Matrix(m)
        μ = mean(x -> !iszero(x), md)
        return μ, 0.0
    else
        nsample >= 2 || throw(ArgumentError("number of samples should be no less than 2"))
        sample_column_list = sample(rng, 1:dim, nsample; replace=false)
        μ, σ = mean_and_std(column_density(col) for col in sample_column_list)
        return μ, σ * sqrt((dim - nsample) / ((dim - 1) * nsample))
    end
end



function estimate_density_raw(
    rng::AbstractRNG,
    m::AbstractOperatorRepresentation{T};
    nsample::Integer=min(10, size(m, 2)),
    tol::Real=Base.rtoldefault(real(T))
) where {T}
    dim = size(m, 2)
    vals = Dict{Int, T}()
    function column_density(col::Integer)
        empty!(vals)
        for (i, v) in get_column_iterator(m, col)
            vals[i] = get(vals, i, zero(T)) + v
        end
        choptol!(vals, tol)
        return length(vals) / dim
    end

    if dim < 10
        md = Matrix(m)
        μ = mean(x -> !iszero(x), md)
        return μ, 0.0
    else
        nsample >= 2 || throw(ArgumentError("number of samples should be no less than 2"))
        sample_column_list = sample(rng, 1:dim, nsample; replace=false)
        μ, σ = mean_and_std(column_density(col) for col in sample_column_list)
        return μ, σ * sqrt((dim - nsample) / ((dim - 1) * nsample))
    end
end


function compute_kagome_bfg(
    Jperp::Tuple{<:Real, <:Real}, Jz::Real,
    shape::AbstractMatrix{<:Integer},
    target_spinflips::AbstractVector{<:Integer},
    nev::Integer,
    nevt::Integer,
    seed::Integer,
    force::Bool=true,
    method::Symbol=:iterative,
    krylovdim::Integer=100,
)
    fail_count = 0

    if nevt < 2*nev
        nevt = 2 * nev + 2
    end

    Jperpmin, Jperpmax = Jperp

    @mylogmsg "lattice shape: $shape"
    @mylogmsg "Jperp: $Jperp"
    @mylogmsg "Jz: $Jz"

    n11 = shape[1,1]
    n12 = shape[1,2]
    n21 = shape[2,1]
    n22 = shape[2,2]
    shape_str = "($n11,$n21)x($n12,$n22)"

    kagome = KagomeIce.make_kagome_lattice(shape)
    lattice = kagome.lattice

    n_sites = length(kagome.lattice.supercell.sites)
    @mylogmsg "Number of sites: $n_sites"

    if n_sites <= 64
        BR = UInt64
    elseif n_sites <= 128
        BR = UInt128
    elseif n_sites <= 256
        BR = UInt256
    elseif n_sites <= 512
        BR = UInt512
    elseif n_sites <= 1024
        BR = UInt1024
    else
        error("Too many sites to be reprented with unsigned integer")
    end

    hs, pauli = QuantumHamiltonian.Toolkit.spin_half_system(n_sites, BR)

    rng = Random.MersenneTwister(seed)
    @mylogmsg "Generating Hamiltonian"
    jx, jy, jz = NullOperator(), NullOperator(), NullOperator()
    for hexa in kagome.motifs.hexagons
        n = length(hexa)
        @assert n == 6
        for i1 in 1:6, i2 in (i1+1):6
            i, j = first(hexa[i1]), first(hexa[i2])
            ampl = rand(rng, Float64) * (Jperpmax - Jperpmin) + Jperpmin
            jx += pauli(i, :x) * pauli(j, :x) * ampl
            jy += pauli(i, :y) * pauli(j, :y) * ampl
            jz += pauli(i, :z) * pauli(j, :z) * Jz
        end
    end
    hamiltonian = simplify(jx + jy + jz) * 0.25
    hamiltonian_classical = simplify(jz) * 0.25

    @mylogmsg "Quantum numbers: $(quantum_number_sectors(hs))"

    let qn = 0
        Sz = qn / 2
        @mylogmsg "spin_z: $Sz"

        @mylogmsg "Creating Hilbert space sector"

        hss = HilbertSpaceSector(hs, qn)
        hssr = represent_dict(hss)

        for spinflip in target_spinflips
            @mylogmsg "spinflip: $spinflip"

            symops_and_amplitudes = [(GlobalBitFlip(false), float(1)), (GlobalBitFlip(true), float(spinflip))]
            rhssr = symmetry_reduce(hssr, symops_and_amplitudes)

            hilbert_space_dimension = dimension(rhssr)
            @mylogmsg "Hilbert space dimension: $(hilbert_space_dimension)"

            hilbert_space_dimension == 0 && continue

            parameter_filename = Dict{Symbol, Any}(
                :shape => shape_str,
                :Jperp=>format("({:.3f},{:.3f})", Jperpmin, Jperpmax),
                :Jz=>format("{:.3f}", Jz),
                :Sz=>format("{:.1f}", Sz),
                :spinflip=>spinflip,
                :seed=>seed,
            )
            matrix_type = "sparse-$method"

            output_filename = savename("gap-$matrix_type", parameter_filename, "msgpack.xz")
            output_filepath = datadir("kagome-xxz-disordered-gap", "($n11,$n21)x($n12,$n22)", output_filename)

            work_filename = savename("gap-$matrix_type", parameter_filename, "work.msgpack.xz")
            work_filepath = datadir("kagome-xxz-disordered-gap", "($n11,$n21)x($n12,$n22)", work_filename)


            if ispath(output_filepath)
                @mylogmsg "File $output_filepath exists."
                if force
                    @mylogmsg "Overwriting."
                else
                    @mylogmsg "Skipping." 

                    if isfile(work_filepath)
                        @mylogmsg "deleting work file $work_filepath"
                        try
                            rm(work_filepath)
                        catch e
                            @mylogmsg "Failed: $e"
                        end
                    end
 
                    continue
                end
            end
            @mylogmsg "Will save results to $output_filepath"
            @mylogmsg "matrix type: $matrix_type"

            # spin_squared_rep = represent(rhssr, spin_squared)
            hamiltonian_classical_rep = represent(rhssr, hamiltonian_classical)
            hamiltonian_rep = represent(rhssr, hamiltonian)

            onevec = ones(Float64, hilbert_space_dimension)
            onevec_energy = round.(real.(hamiltonian_classical_rep * onevec); digits=27, base=2)
            manifold_energy_list = unique(sort(onevec_energy))
            @assert length(manifold_energy_list) >= 2

            ice_manifold_energy = manifold_energy_list[1]
            ice_plus_one_manifold_energy = manifold_energy_list[2]
            @assert ice_plus_one_manifold_energy ≈ ice_manifold_energy + Jz

            selective_indices = (onevec_energy .== ice_manifold_energy) .| (onevec_energy .== ice_plus_one_manifold_energy)

            @mylogmsg "ice manifold energy: $ice_manifold_energy"
            @mylogmsg "excitation manifold energy: $ice_plus_one_manifold_energy"

            let
                μ, σ = estimate_density(Random.GLOBAL_RNG, hamiltonian_rep)
                @mylogmsg "density of H ~ $(string_error(μ, σ))"
            end

            # ======= BEGIN define make_proxy_hamiltonian, proxy_which_eigenvalues
            if method == :square
                @mylogmsg "Square minimization"
                hamiltonian_rep_map = LinearMap{Float64}(vec -> hamiltonian_rep*vec, hilbert_space_dimension, hilbert_space_dimension; issymmetric=true, ishermitian=true, isposdef=false)
                make_proxy_hamiltonian = function (te::Number)
                    return (hamiltonian_rep_map - te * I) * (hamiltonian_rep_map - te * I) + I
                end
            elseif method == :splu
                @mylogmsg "SuperLU factorization"
                iden = PureOperator{Float64, UInt64}(0x0, 0x0, 0x0, 1.0)
                make_proxy_hamiltonian = function (te::Number)
                    @mylogmsg "sparse representing shifted hamiltonian"
                    hamiltonian_rep_shift_sparse = sparse(represent(rhssr, hamiltonian - te * iden))
                    @mylogmsg "SuperLU factorization of sparse hamiltonian"
                    hamiltonian_rep_shift_splu = splu(hamiltonian_rep_shift_sparse)
                    return LinearMap{Float64}(hilbert_space_dimension; issymmetric=true, ismutating=true, isposdef=false) do x, b
                        x[:] = hamiltonian_rep_shift_splu \ b
                        return x
                    end
                end
            elseif method == :iterative
                @mylogmsg "Method: gmres"
                iden = PureOperator{Float64, UInt64}(0x0, 0x0, 0x0, 1.0)
                make_proxy_hamiltonian = function (te::Number)
                    hamiltonian_rep_shift = represent(rhssr, hamiltonian - te * iden)
                    return LinearMap{Float64}(hilbert_space_dimension; issymmetric=true, ismutating=true, isposdef=false) do x, b
                        fill!(x, zero(eltype(x)))
                        result, history = IterativeSolvers.gmres!(x, hamiltonian_rep_shift, b; initially_zero=true, log=true)
                        history.isconverged || error("gmres! not converged: $history")
                        return x
                    end
                end
            else
                @mylogmsg "unsupported method"
                exit(1)
            end
            # ======= END define make_proxy_hamiltonian, proxy_which_eigenvalues

            # ======= BEGIN define compute_eigen
            v0 = zeros(Float64, hilbert_space_dimension)
            if method == :square
                compute_eigen = function (te::Number, nev::Integer)
                    proxy_hamiltonian = make_proxy_hamiltonian(te)
                    converged = false
                    eigenvectors = nothing
                    for itrial in 1:3
                        try
                            randn!(view(v0, selective_indices))
                            normalize!(v0)
                            @mylogmsg "Computing eigenvectors of proxy Hamiltonian"
                            res = eigsolve(
                                x -> proxy_hamiltonian * x,
                                v0,
                                nev,
                                :SR,
                                Lanczos(
                                    verbosity=2,
                                    tol=Base.rtoldefault(Float64),
                                    maxiter=1000,
                                    krylovdim=min(krylovdim, hilbert_space_dimension),
                                )
                            )
                            eigenvectors = hcat(res[2]...)
                            converged = true
                            break
                        catch e
                            @mylogmsg "exception: $e"
                            continue
                        end
                    end
                    converged || error("did not converge")
                    @mylogmsg "Computing eigenvalues and eigenvectors of H"
                    eigenvalues = reeigen(Hermitian(hamiltonian_rep), eigenvectors)
                    return eigenvalues
                end
            elseif method == :iterative || method == :splu
                compute_eigen = function (te::Number, nev::Integer)
                    proxy_hamiltonian = make_proxy_hamiltonian(te)
                    converged = false
                    eigenvectors = nothing                    
                    for itrial in 1:3
                        try
                            randn!(view(v0, selective_indices))
                            normalize!(v0)
                            @mylogmsg "Computing eigenvectors of proxy Hamiltonian"
                            _, eigenvectors = eigs(proxy_hamiltonian; nev=nev, v0=v0, which=:BE, tol=Base.rtoldefault(Float64), maxiter=1000, ncv=min(krylovdim, hilbert_space_dimension))
                            converged = true
                            break
                        catch e
                            @mylogmsg "exception: $e"
                            continue
                        end
                    end
                    converged || error("did not converge")
                    @mylogmsg "Computing eigenvalues and eigenvectors of H"
                    eigenvalues = reeigen(Hermitian(hamiltonian_rep), eigenvectors)
                    return eigenvalues
                end
            else
                @mylogmsg "unsupported method"
                exit(1)
            end
            # ======= END define compute_eigen

            eigenvalues = Float64[]

            if isfile(work_filepath)
                try
                    @mylogmsg "Reading temporary file $work_filepath"
                    open(work_filepath, "r") do io
                        ioc = XzDecompressorStream(io)
                        temp_data = MsgPack.unpack(ioc)
                        eigenvalues2 = Float64[x for x in temp_data["eigenvalue"]]
                        close(ioc)
                        if 2*nev > length(eigenvalues2)
                            error("Required nev larger than that of temporary file. Should recompute eigenvalues")
                        end
                        eigenvalues = eigenvalues2
                    end
                    @mylogmsg "Successful. Eigenvalues: $eigenvalues"
                catch e
                    @mylogmsg "Failed. $e"
                end
            end

            true_target_energy = (ice_manifold_energy + ice_plus_one_manifold_energy) * 0.5
            target_energy = true_target_energy
            @mylogmsg "target energy: $true_target_energy"

            if isempty(eigenvalues)
                try
                    @mylogmsg "Computing eigenvalues"
                    eigenvalues = compute_eigen(target_energy, nevt)
                    @mylogmsg "Eigenvalues: $eigenvalues"
                catch e
                    @mylogmsg "Skipping due to error: $e"
                    continue
                end
                temp_data = OrderedDict(
                    "nev" => nev,
                    "eigenvalue" => eigenvalues
                )
                @mylogmsg "Saving to temporary file $work_filepath"
                let output_directory = dirname(work_filepath)
                    if !isdir(output_directory)
                        mkpath(output_directory)
                    end
                end
                open(work_filepath, "w") do io
                    ioc = XzCompressorStream(io)
                    MsgPack.pack(ioc, temp_data)
                    close(ioc)
                end
            end
            
            @mylogmsg "Eigenvalues: $eigenvalues"
            lower_eigenvalues = eigenvalues[eigenvalues .< true_target_energy]
            higher_eigenvalues = eigenvalues[eigenvalues .>= true_target_energy]
            
            @mylogmsg "lower eigenvalues: $lower_eigenvalues"
            @mylogmsg "higher eigenvalues: $higher_eigenvalues"

            lumo = NaN
            homo = NaN
            try
                # crawl down
                for icrawl in 1:3
                    length(lower_eigenvalues) >= nev && break
                    if !isempty(lower_eigenvalues)
                        target_energy = maximum(lower_eigenvalues)
                    elseif !isempty(higher_eigenvalues)
                        target_energy = target_energy + (target_energy - maximum(higher_eigenvalues))
                    else
                        error("both lower_eigenvalues and higher_eigenvalues are empty")
                    end
                    @mylogmsg "Retargeting lower eigenvalues with $target_energy"
                    eigenvalues = compute_eigen(target_energy, nevt)
                    @mylogmsg "Eigenvalues: $eigenvalues"
                    lower_eigenvalues = eigenvalues[eigenvalues .< true_target_energy]
                    @mylogmsg "lower eigenvalues: $lower_eigenvalues"
                end
                length(lower_eigenvalues) >= nev || error("failed to find $nev lower_eigenvalues")
                homo = maximum(lower_eigenvalues)
                @mylogmsg "HOMO: $homo"
                
                # crawl up
                for icrawl in 1:3
                    length(higher_eigenvalues) >= nev && break
                    if !isempty(higher_eigenvalues)
                        target_energy = minimum(higher_eigenvalues)
                    elseif !isempty(lower_eigenvalues)
                        target_energy = target_energy + (target_energy - minimum(lower_eigenvalues))
                    else
                        error("both lower_eigenvalues and higher_eigenvalues are empty")
                    end
                    @mylogmsg "Retargeting higher eigenvalues with $target_energy"
                    eigenvalues = compute_eigen(target_energy, nevt)
                    @mylogmsg "Eigenvalues: $eigenvalues"
                    higher_eigenvalues = eigenvalues[eigenvalues .>= true_target_energy]
                    @mylogmsg "higher eigenvalues: $higher_eigenvalues"
                end
                length(higher_eigenvalues) >= nev || error("failed to find $nev higher_eigenvalues")
                lumo = minimum(higher_eigenvalues)
                @mylogmsg "LUMO: $lumo"
            catch e
                @mylogmsg "Skipping due to error: $e"
                fail_count += 1
                continue
            end

            energy_gap = lumo - homo
            @mylogmsg "gap: $energy_gap"

            save_data = OrderedDict(
                "parameter" => OrderedDict(
                    "Jperp" => Jperp,
                    "Jz" => Jz,
                    "shape" => [shape[1,1], shape[2,1], shape[1,2], shape[2,2]],
                    "seed"=>seed,
                ),
                "sector" => OrderedDict(
                    "Sz" => Sz,
                    "spinflip" => spinflip,
                    "dimension" => hilbert_space_dimension,
                    "matrix_type" => matrix_type,
                ),
                "ice_manifold_energy" => ice_manifold_energy,
                "ice_plus_one_manifold_energy" => ice_plus_one_manifold_energy,
                "target_energy" => true_target_energy,
                "lower_eigenvalue" => lower_eigenvalues,
                "higher_eigenvalue" => higher_eigenvalues,
                "description"=>"Contains a few eigenstates centered around the midpoint between the lowest energy (ice) manifold and the higher one"
            )

            @mylogmsg "# Saving to $output_filepath"
            let output_directory = dirname(output_filepath)
                if !isdir(output_directory)
                    mkpath(output_directory)
                end
            end
            open(output_filepath, "w") do io
                ioc = XzCompressorStream(io)
                MsgPack.pack(ioc, save_data)
                close(ioc)
            end
            @mylogmsg "Deleting work file $work_filepath"
            try
                rm(work_filepath)
            catch e
                @mylogmsg "Failed: $e"
            end
        end # for spinflip
    end # for (qn,)
    return fail_count
end


function parse_commandline()
    s = ArgParse.ArgParseSettings()
    ArgParse.@add_arg_table! s begin
        "shape"
            arg_type = String
            help = "shape of the lattice in the format (?,?)x(?,?)"
            required = true
        "--Jperp"
            arg_type = Float64
            nargs = 2
            required = true
        "--Jz"
            arg_type = Float64
            default = 1.0
        "--spinflip"
            arg_type = Int
            nargs = '*'
            default = [-1, 1]
        "--nev"
            arg_type = Int
            default = 2
            range_tester = x -> (x>0)
            help = "number of required eigenvalues"
        "--nevt"
            arg_type = Int
            default = 0
            range_tester = x -> (x>=0)
            help = "number of eigenvalues to compute when using sparse"
        "--seed"
            arg_type = Int
            default = 1
            help = "random seed"
        "--debug", "-d"
            help = "debug"
            action = :store_true
        "--force", "-f"
            help = "force run (overwrite)"
            action = :store_true
        "--method"
            arg_type = String
            range_tester = x -> (x in ["iterative", "splu", "square"])
            default = "square"
        "--krylovdim"
            arg_type = Int
            range_tester = x -> (x > 0)
            default = 100
    end
    return ArgParse.parse_args(s)
end

function parse_shape(shape_str::AbstractString)
    shape_pattern = r"\(\s*([-+]?\d+)\s*,\s*([-+]?\d+)\s*\)x\(\s*([-+]?\d+)\s*,\s*([-+]?\d+)\s*\)"
    m = match(shape_pattern, shape_str)
    if isnothing(m)
        throw(ArgumentError("shape should be in format (n11,n12)x(n21,n22)"))
    end
    n11, n21, n12, n22 = [parse(Int, x) for x in m.captures]
    return [n11 n12;
            n21 n22]
end


function main()

    parsed_args = parse_commandline()

    if parsed_args["debug"]
        logger = ConsoleLogger(stdout, Logging.Debug; meta_formatter=my_metafmt)
        global_logger(logger)
    else
        logger = ConsoleLogger(stdout, Logging.Info; meta_formatter=my_metafmt)
        global_logger(logger)
    end

    shape = parse_shape(parsed_args["shape"])

    Jperp = parsed_args["Jperp"]
    Jz = parsed_args["Jz"]

    nev = parsed_args["nev"]
    nevt = parsed_args["nevt"]
    seed = parsed_args["seed"]
    force = parsed_args["force"]
    if parsed_args["method"] == "iterative"
        method_symbol = :iterative
    elseif parsed_args["method"] == "splu"
        method_symbol = :splu
    elseif parsed_args["method"] == "square"
        method_symbol = :square
    else
        method_symbol = :undefined
    end
    krylovdim = parsed_args["krylovdim"]
    target_spinflips = parsed_args["spinflip"]
    @show target_spinflips

    r = compute_kagome_bfg(
        tuple(Jperp...), Jz,
        shape,
        target_spinflips,
        # Sz_list,
        nev,
        nevt,
        # measure,
        seed,
        force,
        method_symbol,
        krylovdim,
    )
    exit(r)
end

main()
