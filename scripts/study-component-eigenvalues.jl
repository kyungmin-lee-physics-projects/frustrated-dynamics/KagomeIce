using SparseArrays
using LinearAlgebra
using Random
using Plots
import PyPlot
using Formatting


using LatticeTools
using QuantumHamiltonian
using KagomeIce

using GraphPlot
using LightGraphs
using PyCall

using Compose, Cairo

global const npl = pyimport("numpy.linalg")



function get_spacings(values, precision::Integer=8)
    values2 = round.(sort(values); digits=precision)
    spacings = values2[2:end] - values2[1:end-1]
    return spacings
end


function get_ratios(values, precision::Integer=8)
    spacings = get_spacings(values, precision)
    n = length(spacings)
    if n <= 3
        Float64[]
    end

    m = n
    while (m > 1) && (spacings[m] == 0)
        m -= 1
    end

    if m <= 1
        return Float64[]
    end

    s_prev = spacings[m] # nonzero
    ratios = Float64[]
    while m > 1
        m -= 1
        s = spacings[m]
        r = min(s, s_prev) / max(s, s_prev)
        if s != 0
            s_prev = s
        end
        push!(ratios, r)
    end
    return ratios
end


struct FakeIrrepComponent <: AbstractSymmetryIrrepComponent end


function compute_eigenvalues(basis_shortlist::AbstractVector{BR}) where {BR<:Unsigned}
    ComplexType = ComplexF64

    mask = make_bitmask(n_sites, BR)
    @assert count_ones(mask) == n_sites
    spinflip(bvec::BR) = mask & (~bvec)

    hsr = represent_dict(hilbert_space, basis_shortlist)
    HSR = typeof(hsr)
    n_basis = dimension(hsr)

    sector_eigenvalues = Vector{Float64}[]

    for spin_flip_sign in [1, -1]
        basis_mapping_representative = Vector{Int}(undef, n_basis)
        fill!(basis_mapping_representative, -1)
        basis_mapping_amplitude = zeros(Float64, n_basis)

        group_size = 2
        symops_and_amplitudes = [(identity, 1), (spinflip, spin_flip_sign)]
        is_identity = [true, spin_flip_sign == 1]

        reduced_basis_list = BR[]
        sizehint!(reduced_basis_list, n_basis ÷ 2 + n_basis ÷ 4)

        visited = falses(n_basis)

        basis_states = Vector{BR}(undef, group_size)
        basis_amplitudes = Dict{BR, ComplexType}()
        sizehint!(basis_amplitudes, 2)

        for ivec_p in 1:n_basis
            visited[ivec_p] && continue
            bvec = hsr.basis_list[ivec_p]

            compatible = true
            for i in 2:2
                (symop, _) = symops_and_amplitudes[i]
                bvec_prime = symop(bvec)
                if bvec_prime < bvec
                    compatible = false
                    break
                elseif bvec_prime == bvec && !is_identity[i]
                    compatible = false
                    break
                end
                basis_states[i] = bvec_prime
            end
            (!compatible) && continue
            basis_states[1] = bvec

            push!(reduced_basis_list, bvec)

            empty!(basis_amplitudes)
            for i in 1:group_size
                (_, ampl) = symops_and_amplitudes[i]
                bvec_prime = basis_states[i]
                basis_amplitudes[bvec_prime] = ampl
            end
            inv_norm = inv(sqrt(float(length(basis_amplitudes))))
            for (bvec_prime, amplitude) in basis_amplitudes
                ivec_p_prime = hsr.basis_lookup[bvec_prime]
                visited[ivec_p_prime] = true
                basis_mapping_representative[ivec_p_prime] = ivec_p
                basis_mapping_amplitude[ivec_p_prime] = amplitude * inv_norm
            end
        end

        basis_mapping_index = Vector{Int}(undef, n_basis)
        fill!(basis_mapping_index, -1)

        for (ivec_r, bvec) in enumerate(reduced_basis_list)
            ivec_p = hsr.basis_lookup[bvec]
            basis_mapping_index[ivec_p] = ivec_r
        end

        for (ivec_p_prime, ivec_p) in enumerate(basis_mapping_representative)
            (ivec_p <= 0) && continue
            (ivec_p_prime == ivec_p) && continue
            ivec_r = basis_mapping_index[ivec_p]
            basis_mapping_index[ivec_p_prime] = ivec_r
        end

        RHSR = ReducedHilbertSpaceRepresentation{
            HSR,
            FakeIrrepComponent,
            BR,
            ComplexType
        }

        rhsr = RHSR(
            hsr, FakeIrrepComponent(),
            reduced_basis_list, basis_mapping_index, basis_mapping_amplitude,
        )
        @show dimension(rhsr)
        eigenvalues = npl.eigvalsh(Matrix(represent(rhsr, hamiltonian)))
        push!(sector_eigenvalues, eigenvalues)
    end
    return sector_eigenvalues
end





function main()
    # shape = [
    #     3  1;
    #     -1  3;
    # ]
    shape = [2 2;
            -2 4]

    shape_str = "($(shape[1,1]),$(shape[2,1]))x($(shape[1,2]),$(shape[2,2]))"

    kagome = KagomeIce.make_kagome_lattice(shape)

    n_triangles = length(kagome.nearest_neighbor_triangles)
    n_sites = numsite(kagome.lattice.supercell)

    basis_list = KagomeIce.BalentsFisherGirvin.generate_basis(kagome)
    sort!(basis_list)

    (hilbert_space, spin_operator) = QuantumHamiltonian.Toolkit.spin_system(n_sites, 1//2)
    hamiltonian_terms = KagomeIce.BalentsFisherGirvin.generate_hamiltonian(kagome)

    rng = MersenneTwister(1)

    hamiltonian = let n = length(hamiltonian_terms),
        magnitude = rand(rng, Float64, n),
        phase = 1

        amplitude = magnitude .* phase
        h = sum(amplitude  .* hamiltonian_terms)
        h + conj(transpose(h))
    end

    full_hilbert_space_representation = represent_dict(hilbert_space, basis_list)

    hamiltonian_representation = represent(full_hilbert_space_representation, hamiltonian)

    @show dimension(full_hilbert_space_representation)
    ndim = dimension(full_hilbert_space_representation)

    graph = SimpleGraph(ndim)
    for irow in 1:ndim
        ψ = SparseState(Dict(get_row_iterator(hamiltonian_representation, irow)))
        choptol!(ψ, Base.rtoldefault(Float64))
        for (icol, _) in ψ.components
            add_edge!(graph, irow, icol)
        end
    end

    # add spin flips
    # for irow in 1:ndim
    #     brow = hamiltonian_representation.hilbert_space_representation.basis_list[irow]
    #     mask = make_bitmask(n_sites, typeof(brow))
    #     bcol = mask & (~brow)
    #     icol = hamiltonian_representation.hilbert_space_representation.basis_lookup[bcol]
    #     add_edge!(graph, irow, icol)
    # end


    compos = connected_components(graph)




    if false
        sector_eigenvalues = Vector{Float64}[]
        for compo in compos
            append!(sector_eigenvalues, compute_eigenvalues(basis_list[compo]))
        end
    end

    if false

        fig = PyPlot.figure(figsize=(4, 4))

        for compo in compos
            if length(compo) != 1
                continue
            end
            ivec = first(compo)
            bvec = basis_list[ivec]
            #fig = plot(title="ice configuration #$ivec", legend=false, size=(400, 400))
            fig.clf()
            ax = fig.gca()
            ax.set_title("ice configuration #$ivec")

            for (isite, ((sitetype, siteuc), sitefc)) in enumerate(kagome.lattice.supercell.sites)
                sitecc = fract2carte(kagome.lattice.supercell, sitefc)
                x = sitecc[1]
                y = sitecc[2]
                xs = Float64[]
                ys = Float64[]
                for i1 in -1:1, i2 in -1:1
                    if (i1, i2) == (0,0)
                        continue
                    end
                    R = kagome.lattice.supercell.latticevectors * [i1, i2]
                    push!(xs, x + R[1])
                    push!(ys, y + R[2])
                end

                if (bvec >> (isite-1)) & 0x1 == 0x0
                    # scatter!(xs, ys, markercolor="black", markerstyle=:circle, markersize=8, alpha=0.2)
                    # scatter!([x], [y], markercolor="black", markerstyle=:circle, markersize=8, alpha=1)
                    ax.scatter(xs, ys, 64, color="black", marker="o", alpha=1, edgecolors="black")
                    ax.scatter([x], [y], 64, color="black", marker="o", alpha=1, edgecolors="black")
                else
                    #scatter!(xs, ys, markercolor="white", markerstyle=:circle, markersize=8, alpha=0.2)
                    #scatter!([x], [y], markercolor="white", markerstyle=:circle, markersize=8, alpha=1)
                    ax.scatter(xs, ys, 64, color="white", marker="o", alpha=1, edgecolors="black", linewidths=0.5)
                    ax.scatter([x], [y], 64, color="white", marker="o", alpha=1, edgecolors="black", linewidths=0.5)
                end
            end
            #plot!(xlim=[-1, 2], ylim=[-1, 2])
            ax.set_xlim(-1, 3)
            ax.set_ylim(-1, 3)
            ax.set_aspect(1.0)

            ax.set_xticks([])
            ax.set_yticks([])
            fig.savefig(format("ice-configuration-frozen_{:03d}.png", ivec), dpi=300, bbox_inches="tight")
            fig.savefig(format("ice-configuration-frozen_{:03d}.pdf", ivec), dpi=300, bbox_inches="tight")
        end
    end



    if false
        function pdf_general(beta::Real)
            function pdf(x::Real)
                return (27.0/4) * (x+x^2)^(beta) / (1+x+x^2)^(1+3*beta/2)
            end
            function pdf(x::AbstractArray{<:Real})
                return (27.0/4) .* (x + x.^2).^(beta) ./ (1 + x + x .^ 2)^(1 + 3 .* beta./2)
            end
            return pdf
        end

        pdf_goe = pdf_general(1)
        pdf_poisson(x::Real) = 2 / (1+x)^2
        pdf_poisson(x::AbstractArray{<:Real}) = 2 ./ (1+x).^2

        xs = 0:0.01:1
        ys_goe = pdf_goe.(xs)
        ys_poi = pdf_poisson.(xs)

        fig = PyPlot.figure(figsize=(3.5, 3))

        let
            fig.clf()
            ax = fig.gca()

            v = vcat(sector_eigenvalues...)
            r = get_ratios(v)
            if !isempty(r)
                ax.hist(r, bins=32, density=true, range=[0, 1])
                ax.plot(xs, ys_poi, linewidth=2, label="Poisson")
                ax.plot(xs, ys_goe, linewidth=2, label="GOE")

                ax.set_xlim(0, 1)
                ax.set_ylim(0, 2.2)

                ax.set_xlabel(raw"$\tilde{r}$", fontsize=12)
                ax.set_ylabel(raw"$P(\tilde{r})$", fontsize=12)
                ax.legend(loc=1)

                dim = length(v)
                ax.set_title("$shape_str, complete (dim = $dim)", fontsize=8)

                fig.savefig("level-statistics-complete.png", dpi=300, bbox_inches="tight")
                fig.savefig("level-statistics-complete.pdf", dpi=300, bbox_inches="tight")
            end
        end


        let
            fig.clf()
            ax = fig.gca()

            v = vcat(sector_eigenvalues[1:2:end]...)
            r = get_ratios(v)
            if !isempty(r)
                ax.hist(r, bins=32, density=true, range=[0, 1])
                ax.plot(xs, ys_poi, linewidth=2, label="Poisson")
                ax.plot(xs, ys_goe, linewidth=2, label="GOE")

                ax.set_xlim(0, 1)
                ax.set_ylim(0, 2.2)

                ax.set_xlabel(raw"$\tilde{r}$", fontsize=12)
                ax.set_ylabel(raw"$P(\tilde{r})$", fontsize=12)
                ax.legend(loc=1)

                dim = length(v)
                ax.set_title("$shape_str, Z2 symmetric (dim = $dim)", fontsize=8)

                fig.savefig("level-statistics-z2_symmetric.png", dpi=300, bbox_inches="tight")
                fig.savefig("level-statistics-z2_symmetric.pdf", dpi=300, bbox_inches="tight")
            end
        end
        let
            fig.clf()
            ax = fig.gca()

            v = vcat(sector_eigenvalues[2:2:end]...)
            r = get_ratios(v)
            if !isempty(r)
                ax.hist(r, bins=32, density=true, range=[0, 1])
                ax.plot(xs, ys_poi, linewidth=2, label="Poisson")
                ax.plot(xs, ys_goe, linewidth=2, label="GOE")

                ax.set_xlim(0, 1)
                ax.set_ylim(0, 2.2)

                ax.set_xlabel(raw"$\tilde{r}$", fontsize=12)
                ax.set_ylabel(raw"$P(\tilde{r})$", fontsize=12)
                ax.legend(loc=1)

                dim = length(v)
                ax.set_title("$shape_str, Z2 antisymmetric (dim = $dim)", fontsize=8)

                fig.savefig("level-statistics-z2_antisymmetric.png", dpi=300, bbox_inches="tight")
                fig.savefig("level-statistics-z2_antisymmetric.pdf", dpi=300, bbox_inches="tight")
            end
        end




        for i in eachindex(sector_eigenvalues)
            fig.clf()
            ax = fig.gca()

            r = get_ratios(sector_eigenvalues[i])
            if !isempty(r)
                ax.hist(r, bins=32, density=true, range=[0, 1])
                ax.plot(xs, ys_poi, linewidth=2, label="Poisson")
                ax.plot(xs, ys_goe, linewidth=2, label="GOE")

                ax.set_xlim(0, 1)
                ax.set_ylim(0, 2.2)

                ax.set_xlabel(raw"$\tilde{r}$", fontsize=12)
                ax.set_ylabel(raw"$P(\tilde{r})$", fontsize=12)
                ax.legend(loc=1)

                ax.set_title("$shape_str, sector #$i (dim = $(length(sector_eigenvalues[i])))", fontsize=8)

                fig.savefig("level-statistics-sector-$i.png", dpi=300, bbox_inches="tight")
                fig.savefig("level-statistics-sector-$i.pdf", dpi=300, bbox_inches="tight")
            end
        end


    end
end



# fig = plot(size=(200, 900), legend=false, dpi=600)
# for (i, ev) in enumerate(sector_eigenvalues)
#     scatter!(zeros(Float64, length(ev)) .+ i, ev, markershape=:hline)
# end
# xlabel!("sector")
# ylabel!("eigenvalue")
# fig
# savefig("sector-eigenvalues-(3,-2)x(2,2).png")
