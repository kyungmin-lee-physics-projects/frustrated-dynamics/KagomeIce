using DrWatson
quickactivate(joinpath(@__DIR__, ".."))

using MsgPack
using CodecXz
using PyPlot
using PyCall
mpl = pyimport("matplotlib")
using Glob

include(srcdir("levelstatistics.jl"))

# shape_str = "(3,0)x(1,2)"
shape_str = "(2,0)x(0,2)"

filename_pattern = "spectrum-dense_Jperp=(-*,*)_Jz=1.000_Sz=0.0_seed=*_shape=$(shape_str)_spinflip=*.msgpack.xz"
filepaths = Glob.glob(filename_pattern, datadir("kagome-xxz-disordered/$(shape_str)"))
database = Dict{Tuple{Float64, Int}, Vector{Vector{Float64}}}()
for filepath in filepaths
    open(filepath, "r") do io
        ioc = XzDecompressorStream(io)
        item = MsgPack.unpack(ioc)
        Jperpmax = item["parameter"]["Jperp"][2]
        spinflip = item["sector"]["spinflip"]
        if haskey(database, (Jperpmax, spinflip))
            push!(database[(Jperpmax, spinflip)], item["eigenvalue"])
        else
            database[(Jperpmax, spinflip)] = [item["eigenvalue"]]
        end
        close(ioc)
    end
end

Jperpmax_list = unique(sort([x for (x,y) in keys(database)]))
spinflip_list = unique(sort([y for (x,y) in keys(database)]))

# xs = 0:0.01:1
# ys_goe = pdf_goe.(xs)
# ys_poi = pdf_poisson.(xs)

for Jperpmax in Jperpmax_list, spinflip in spinflip_list
    all_eigenvalues = database[Jperpmax, spinflip]
    all_values_ice = Float64[]
    all_values_complete = Float64[]
    for eigenvalues in all_eigenvalues
        append!(all_values_ice, eigenvalues[1:268])
        append!(all_values_complete, eigenvalues)
    end
    fig = PyPlot.figure(figsize=(4, 3.5))

    ax = fig.gca()
    _, _, p2 = ax.hist(all_values_complete, bins=256, density=true, alpha=0.5, color="xkcd:orangered", label="complete");
    ax.set_title("\$J_{\\perp} = $Jperpmax\$")

    fig.savefig(plotsdir("density-of-states_xxz_$(shape_str)_$(Jperpmax).pdf"), dpi=300, bbox_inches="tight")
end
PyPlot.show()

# gaps = []
# for Jperpmax in Jperpmax_list, spinflip in spinflip_list
#     all_eigenvalues = database[Jperpmax, spinflip]
#     values_ice = Float64[]
#     values_above = Float64[]
#     for eigenvalues in all_eigenvalues
#         push!(values_ice, eigenvalues[268])
#         push!(values_above, eigenvalues[269])
#     end
#     push!(gaps, (Jperpmax, minimum(values_above) - maximum(values_ice)))
# end
    
# fig = PyPlot.figure(figsize=(4, 3.5))

# linestyles = ["-", "--"]
# colors=["xkcd:dark orange", "xkcd:hunter green"]
# for (i, Jperpmax) in enumerate([0.05, 0.2])
#     spinflip = 1
#     all_eigenvalues = database[Jperpmax, spinflip]
#     all_values_complete = vcat(all_eigenvalues...)

#     ax = fig.gca()
#     (h, bin_edges) = np.histogram(all_values_complete, bins=1000, range=(-6, 9), density=true)
#     h = h ./ maximum(h)
    
#     ax.plot(
#         0.5*(bin_edges[1:end-1] + bin_edges[2:end]),
#         h,
#         ls=linestyles[i],
#         color=colors[i],
#         linewidth=1.5,
#         label="\$J=$Jperpmax\$")
# end
# ax.set_ylim(-0.01, 1.65)
# ax.set_xlim(-5.5, 9.5)
# ax.set_yticks([0])
# PyPlot.setp(ax.get_xticklabels(), fontsize=12)
# PyPlot.setp(ax.get_yticklabels(), fontsize=12)

# ax.set_ylabel("\$\\rho(E)\$ (arb. unit)", fontsize=16)
# ax.set_xlabel(raw"$E$", fontsize=16)
# l = ax.legend(
#     [raw"$0.05$", raw"$0.2$"],
#     bbox_to_anchor=(0.1, 0.5, 0.32, 0.28), bbox_transform=ax.transAxes,
#     title=raw"$J=$",
#     fontsize=14,
#     frameon=false,
#     title_fontsize=14,
#     labelspacing=0.15,
# )

# ax.annotate("ice", xy=(-4.6, 0.15), xytext=(-4.6, 0.4), arrowprops=Dict("arrowstyle"=>"simple", "color"=>"red"), ha="center", color="red", fontsize=18, weight="bold")
# ax.set_xlim(-5.25, 0)
# axins = inset_locator.inset_axes(ax, width="100%", height="100%", bbox_to_anchor=(.67, .77, .30, .23), bbox_transform=ax.transAxes)

# axins.plot(xs, 1 .- 8.3xs, "k", linewidth=1.5, linestyle=(0, (1, 1)))
# axins.plot([x for (x, y) in gaps], [y for (x, y) in gaps], "o", alpha=1)
# axins.set_xticks([0, 0.1, 0.2])
# axins.set_xticklabels(["0", "0.1", "0.2"], fontsize=12)
# axins.set_yticklabels(["0", "1"], fontsize=12)
# axins.set_ylim(0, 1.1)
# axins.set_xlim(0, 0.2)
# axins.set_xlabel(raw"$J$", fontsize=14)
# axins.set_ylabel(raw"$\Delta_{\mathrm{ice}}$", fontsize=14)
# fig.savefig("density-of-states-bfg.pdf", dpi=300, bbox_inches="tight")
