# Need to take Z2 into account

using DrWatson
quickactivate(joinpath(@__DIR__, ".."))

using Logging

using LinearAlgebra
using SparseArrays
using Printf
using DataStructures
using BitIntegers

import ArgParse
using KrylovKit
using LinearMaps

using LatticeTools
using QuantumHamiltonian
using KagomeIce

using CodecXz
using MsgPack
using Formatting

using ProgressMeter


using PyCall
npl = pyimport("numpy.linalg")

function compute_kagome_bfg(
    Jx::Real, Jy::Real, Jz::Real,
    shape::AbstractMatrix{<:Integer},
    target_state_vec_list::AbstractVector{<:CartesianIndex},
    dt::Number, # Δt
    nt::Integer, # number of time slices total! (that is, nt + 1 including t=0)
    tsym_irrep_indices::AbstractVector{<:Integer},
    psym_irrep_indices::AbstractVector{<:Integer},
    psym_irrep_components::AbstractVector{<:Integer},
    force::Bool=true
)
    @mylogmsg "lattice shape: $shape"
    @mylogmsg "Jx: $Jx"
    @mylogmsg "Jy: $Jy"
    @mylogmsg "Jz: $Jz"
    # @mylogmsg "Sz: $Szs"

    n1a = shape[1,1]
    n1b = shape[2,1]
    n2a = shape[1,2]
    n2b = shape[2,2]
    shape_str = "($n1a,$n1b)x($n2a,$n2b)"

    kagome = KagomeIce.make_kagome_lattice(shape)
    lattice = kagome.lattice

    n_sites = length(kagome.lattice.supercell.sites)
    @mylogmsg "Number of sites: $n_sites"
    for target_state_vec in target_state_vec_list
        if length(target_state_vec) != n_sites
            error("target_state length $(length(target_state_vec)) does not match number of sites ($n_sites)")
        end
    end

    if n_sites <= 64
        BR = UInt64
    elseif n_sites <= 128
        BR = UInt128
    elseif n_sites <= 256
        BR = UInt256
    elseif n_sites <= 512
        BR = UInt512
    elseif n_sites <= 1024
        BR = UInt1024
    else
        error("Too many sites to be reprented with unsigned integer")
    end

    hs, pauli = QuantumHamiltonian.Toolkit.spin_half_system(n_sites, BR)
    qns = Int[]
    target_states = BR[]
    for target_state_vec in target_state_vec_list
        target_state = compress(hs, target_state_vec)
        qn = get_quantum_number(hs, target_state)[1]
        push!(qns, qn)
        push!(target_states, target_state)
    end
    sort!(qns)
    unique!(qns)

    @mylogmsg "target states: $target_states"
    @mylogmsg "target quantum numbers: $qns"
    
    @mylogmsg "Generating Hamiltonian"
    jx, jy, jz = NullOperator(), NullOperator(), NullOperator()
    for hexa in kagome.motifs.hexagons
        n = length(hexa)
        @assert n == 6
        for i1 in 1:6, i2 in (i1+1):6
            i, j = first(hexa[i1]), first(hexa[i2])
            jx += pauli(i, :x) * pauli(j, :x)
            jy += pauli(i, :y) * pauli(j, :y)
            jz += pauli(i, :z) * pauli(j, :z)
        end
    end
    hamiltonian = simplify(Jx*jx + Jy*jy + Jz*jz) * 0.25

    @mylogmsg "Quantum numbers: $(quantum_number_sectors(hs))"

    for qn in qns
        Sz = qn / 2
        @mylogmsg "spin_z: $Sz"

        @mylogmsg "Creating Hilbert space sector"
        hss = HilbertSpaceSector(hs, qn)
        @mylogmsg "Representing the Hilbert space sector"
        hssr = represent_dict(hss)
        @mylogmsg "    dimension : $(dimension(hssr))"

        ssymbed = kagome.space_symmetry_embedding

        let
            rhssr = hssr
            valid_target_list = []
            for target_state in target_states
                target_index_parent = hssr.basis_lookup[target_state]
                target_state_string = string(target_state; base=2, pad=n_sites)
                target_state_vec = extract(hs, target_state)                

                parameter_filename = Dict{Symbol, Any}(
                    :shape => shape_str,
                    :Jx=>format("{:.3f}", Jx),
                    :Jy=>format("{:.3f}", Jy),
                    :Jz=>format("{:.3f}", Jz),
                    :target=>target_state_string,
                    # :Sz=>format("{:.1f}", Sz),
                    :dt=>format("{:.6f}", dt),
                    :nt=>nt,
                )
                output_filename = savename("timeevolution-nosymmetry", parameter_filename, "msgpack.xz")
                output_filepath = datadir("kagome-xxz-timeevolution", "($n1a,$n1b)x($n2a,$n2b)", output_filename)
                if ispath(output_filepath)
                    @mylogmsg "File $output_filepath exists."
                    if force
                        @mylogmsg "Overwriting."
                    else
                        @mylogmsg "Skipping."
                        continue
                    end
                end
                push!(
                    valid_target_list,
                    (
                        state=target_state,
                        index_parent=target_index_parent,
                        # index_reduced=target_index_reduced,
                        # amplitude_reduced=target_amplitude_reduced,
                        vec=target_state_vec,
                        string=target_state_string,
                        output_filepath=output_filepath,
                    )
                )
            end
            hilbert_space_dimension = dimension(rhssr)
            @mylogmsg "    Hilbert space dimension    : $(hilbert_space_dimension)"
            hilbert_space_dimension == 0 && continue

            if isempty(valid_target_list)
                @mylogmsg "No overlap with this sector. Skipping."
                continue
            end

            hamiltonian_rep = represent(rhssr, hamiltonian)

            @mylogmsg "# Computing time evolution"

            psi = zeros(ComplexF64, (hilbert_space_dimension, length(valid_target_list)))
            for (itarget, target) in enumerate(valid_target_list)
                psi[target.index_parent, itarget] = 1
            end
            psi0 = zeros(ComplexF64, (hilbert_space_dimension, length(valid_target_list)))
            for (itarget, target) in enumerate(valid_target_list)
                psi0[target.index_parent, itarget] = 1
            end

            times = dt .* (0:nt)
            coefficients = zeros(ComplexF64, (length(valid_target_list), nt+1))
            coefficients[:, 1] .= one(ComplexF64)
            normres = zeros(Float64, (length(valid_target_list), nt+1))

            # U = exp(hamiltonian_rep .* (-im * dt))
            for (it, t) in enumerate(times)
                it == 1 && continue

                for (itarget, target) in enumerate(valid_target_list)
                    v, info = exponentiate(hamiltonian_rep, -im * dt, psi[:, itarget]; ishermitian=true, tol=eps(Float64)*10, verbosity=0, krylovdim=100)
                    # v, info = exponentiate(hamiltonian_rep, -im * t, psi0[:, itarget]; ishermitian=true, tol=1E-14/abs(t))
                    @assert info.converged == 1
                    # v = U * psi[:, itarget]
                    @assert isapprox(norm(v), 1; atol=1E-12)
                    psi[:, itarget] = v ./ norm(v)
                    coefficients[itarget, it] = psi[target.index_parent, itarget]
                    # normres[itarget, it] = info.normres
                end
            end

            for (itarget, target) in enumerate(valid_target_list)
                save_data = OrderedDict(
                    "parameter" => OrderedDict(
                        "Jx" => Jx,
                        "Jy" => Jy,
                        "Jz" => Jz,
                        "shape" => [shape[1,1], shape[2,1], shape[1,2], shape[2,2]],
                        "dt" => dt,
                        "nt" => nt,
                    ),
                    "sector" => OrderedDict(
                        "Sz" => Sz,
                        # "tsym_irrep_index" => ssic.normal.irrep_index,
                        # "psym_irrep_index" => ssic.rest.irrep_index,
                        # "psym_irrep_component" => ssic.rest.irrep_component,
                        "dimension" => hilbert_space_dimension,
                    ),
                    "target_state" => target.vec.I,
                    # "overall_amplitude_real" => real(target.amplitude_reduced),
                    # "overall_amplitude_imag" => imag(target.amplitude_reduced),
                    "times" => times,
                    "amplitude_real" => real.(coefficients[itarget, :]),
                    "amplitude_imag" => imag.(coefficients[itarget, :]),
                    "normres" => normres[itarget, :],
                )

                @mylogmsg "# Saving to $(target.output_filepath)"
                let output_directory = dirname(target.output_filepath)
                    if !isdir(output_directory)
                        mkpath(output_directory)
                    end
                end
                open(target.output_filepath, "w") do io
                    ioc = XzCompressorStream(io)
                    MsgPack.pack(ioc, save_data)
                    close(ioc)
                end
            end # for target_state
        end # for ssic
    end # for (qn,)
end



function parse_commandline()
    s = ArgParse.ArgParseSettings()
    ArgParse.@add_arg_table! s begin
        "shape"
            arg_type = String
            help = "shape of the lattice in the format (?,?)x(?,?)"
            required = true
        "--Jx"
            arg_type = Float64
            nargs = '+'
            default = [1.0]
        "--Jy"
            arg_type = Float64
            nargs = '+'
            default = [1.0]
        "--Jz"
            arg_type = Float64
            nargs = '+'
            default = [1.0]
        "--target"
            arg_type = String
            nargs = '+'
            help = "target state (zero based)"
        "--dt"
            arg_type = Float64
            required = true
            range_tester = x -> (x > 0)
            help = "time step"
        "--nt"
            arg_type = Int
            required = true
            range_tester = x -> (x > 0)
            help = "number of time slices"
        "--tii"
            arg_type = Int
            nargs = '*'
            help = "tii"
        "--pii"
            arg_type = Int
            nargs = '*'
            help = "pii"
        "--pic"
            arg_type = Int
            nargs = '*'
            help = "pic"
        "--max-dense"
            arg_type = Int
            default = 20000
            range_tester = x -> (x>0)
            help = "maximum hilbert space dimension to solve with dense matrix"
        "--nev"
            arg_type = Int
            default = 500
            range_tester = x -> (x>0)
            help = "number of eigenvalues to compute when using sparse"
        "--debug", "-d"
            help = "debug"
            action = :store_true
        "--measure", "-m"
            help = "measure"
            action = :store_true
        "--force", "-f"
            help = "force run (overwrite)"
            action = :store_true
    end
    return ArgParse.parse_args(s)
end

function parse_shape(shape_str::AbstractString)
    shape_pattern = r"\(\s*([-+]?\d+)\s*,\s*([-+]?\d+)\s*\)x\(\s*([-+]?\d+)\s*,\s*([-+]?\d+)\s*\)"
    m = match(shape_pattern, shape_str)
    if isnothing(m)
        throw(ArgumentError("shape should be in format (n1a,n1b)x(n2a,n2b)"))
    end
    n1a, n1b, n2a, n2b = [parse(Int, x) for x in m.captures]
    return [n1a n2a; n1b n2b]
end


function main()

    parsed_args = parse_commandline()

    if parsed_args["debug"]
        logger = ConsoleLogger(stdout, Logging.Debug; meta_formatter=my_metafmt)
        global_logger(logger)
    else
        logger = ConsoleLogger(stdout, Logging.Info; meta_formatter=my_metafmt)
        global_logger(logger)
    end

    shape = parse_shape(parsed_args["shape"])

    Jxs = parsed_args["Jx"]
    Jys = parsed_args["Jy"]
    Jzs = parsed_args["Jz"]

    dt = parsed_args["dt"]
    nt = parsed_args["nt"]

    # Sz_list = parsed_args["Sz"]
    tii_list = parsed_args["tii"]
    pii_list = parsed_args["pii"]
    pic_list = parsed_args["pic"]

    max_dense = parsed_args["max-dense"]
    nev = parsed_args["nev"]
    # measure = parsed_args["measure"]
    force = parsed_args["force"]

    target_state_vec_list = [
        CartesianIndex([parse(Int, c) + 1 for c in reverse(args_target)]...)
        for args_target in parsed_args["target"]
    ]

    for (Jx, Jy, Jz) in Iterators.product(Jxs, Jys, Jzs)
        compute_kagome_bfg(
            Jx, Jy, Jz,
            shape,
            target_state_vec_list,
            dt, nt,
            tii_list, pii_list, pic_list,
            # max_dense,
            # nev,
            # measure,
            force,
        )
    end
end


# function compute_kagome_bfg(
#     Jx::Real, Jy::Real, Jz::Real,
#     shape::AbstractMatrix{<:Integer},
#     target_state_vec_list::AbstractVector{<:CartesianIndex},
#     dt::Number, # Δt
#     nt::Integer, # number of time slices total! (that is, nt + 1 including t=0)
#     tsym_irrep_indices::AbstractVector{<:Integer},
#     psym_irrep_indices::AbstractVector{<:Integer},
#     psym_irrep_components::AbstractVector{<:Integer},
#     force::Bool=true
# )


main()
