using DrWatson
quickactivate(joinpath(@__DIR__, ".."))

using SparseArrays
using LinearAlgebra
using Random
using Formatting
using ArgParse


using LightGraphs

using ProgressMeter

using PyCall
mpl = pyimport("matplotlib")
mpl_backend_pdf = pyimport("matplotlib.backends.backend_pdf")
import PyPlot

using LatticeTools
using QuantumHamiltonian

using KagomeIce


"""
    make_ice_manifold(kagome)

Return `HilbertSpaceRepresentation` of the kagome lattice
"""
function make_ice_manifold(kagome)
    basis_list = KagomeIce.BalentsFisherGirvin.generate_basis(kagome)
    n_sites = numsite(kagome.lattice.supercell)

    (hilbert_space, spin_operator) = QuantumHamiltonian.Toolkit.spin_system(n_sites, 1//2)
    return represent_dict(hilbert_space, basis_list)
end


"""
    make_ice_hamiltonian_graph(kagome, full_hilbert_space_representation)

Return a graph for ice Hamiltonian
"""
function make_ice_hamiltonian_graph(
    kagome,
    full_hilbert_space_representation::HilbertSpaceRepresentation
)
    hamiltonian_terms = KagomeIce.BalentsFisherGirvin.generate_hamiltonian(kagome)
    hamiltonian = let magnitude = 1, phase = 1
        amplitude = magnitude .* phase
        h = sum(amplitude .* hamiltonian_terms)
        h + conj(transpose(h))
    end
    hamiltonian_representation = represent(full_hilbert_space_representation, hamiltonian)

    ndim = dimension(full_hilbert_space_representation)
    @info "ice manifold dimension = $ndim"

    graph = SimpleGraph(ndim)
    for irow in 1:ndim
        ψ = SparseState(Dict(get_row_iterator(hamiltonian_representation, irow)))
        choptol!(ψ, Base.rtoldefault(Float64))
        for (icol, _) in ψ.components
            add_edge!(graph, irow, icol)
        end
    end
    return graph
end



"""
    plot_ice_configurations(kagome, hilbert_space_representation, components, output_filepath, first_only)

Plot ice configurations, grouped by component.
"""
function plot_ice_configurations(
    kagome::NamedTuple,
    full_hilbert_space_representation::HilbertSpaceRepresentation,
    components::AbstractVector{<:AbstractVector{<:Integer}},
    output_filepath::AbstractString,
    first_only::Bool,
)
    hilbert_space = basespace(full_hilbert_space_representation)
    basis_list = full_hilbert_space_representation.basis_list

    # compute site coordinates (common to all configurations)
    xs, ys = Float64[], Float64[]
    for (isite, ((sitetype, siteuc), sitefc)) in enumerate(kagome.lattice.supercell.sites)
        sitecc = fract2carte(kagome.lattice.supercell, sitefc)
        x, y = sitecc
        push!(xs, sitecc[1])
        push!(ys, sitecc[2])
    end
    extents = [
        minimum(xs) - 1.5, maximum(xs) + 1.5,
        minimum(ys) - 1, maximum(ys) + 1
    ]
    w = extents[2] - extents[1]
    h = extents[4] - extents[3]

    markersize = round(Int, 500 / w)

    @info "Saving to $output_filepath"
    fig = PyPlot.figure(figsize=(5, 5 * h / w))
    pdf = mpl_backend_pdf.PdfPages(output_filepath)

    progbar = Progress(first_only ? length(components) : length(basis_list))
    for (icompo, compo) in enumerate(components)
        for ivec in compo
            bvec = basis_list[ivec]

            cs = String[]
            sizehint!(cs, length(xs))

            for isite in eachindex(kagome.lattice.supercell.sites)
                if get_state_index(hilbert_space, bvec, isite) == 1
                    push!(cs, "k")
                elseif get_state_index(hilbert_space, bvec, isite) == 2
                    push!(cs, "w")
                else
                    @show bvec
                    @error "unsupported state index"
                end
            end

            fig.clf()
            ax = fig.gca()
            ax.set_title(
                "component #$icompo/$(length(components)) (size: $(length(compo)))"
                #*" config #$ivec/$(length(basis_list))",
                , fontsize=18
            )
            ax.scatter(
                xs, ys, markersize, cs,
                marker="o", edgecolors="k", alpha=1, linewidths=1
            )

            for i1 in -2:2, i2 in -2:2
                if (i1, i2) != (0, 0)
                    R = kagome.lattice.supercell.latticevectors * [i1, i2]
                    ax.scatter(
                        xs .+ R[1], ys .+ R[2], markersize, cs,
                        marker="o", edgecolors="k", alpha=0.3, linewidths=1
                    )
                end
            end
            ax.set_xlim(extents[1], extents[2])
            ax.set_ylim(extents[3], extents[4])
            ax.set_xticks([])
            ax.set_yticks([])
            ax.set_aspect(1.0)

            fig.tight_layout()
            pdf.savefig(fig)

            next!(progbar)
            if first_only
                break
            end
        end
    end

    pdf.close()
end



function parse_commandline()
    s = ArgParseSettings(
        description="""
        Generate a coloring graph file in Pajek .net format.

        The nodes are labeled by their colorings, and the edges are weighted by the Kempe
        loop length.
        """
    )
    @add_arg_table! s begin
        "shape"
            help = "Shape of the lattice, in format (n11,n12)x(n21,n22)"
            arg_type = String
            required = true
        "--out", "-o"
            help = "Name of the output file. Default is kempe-[shape].net"
            arg_type = String
            required = false
            default = nothing
        "--first-only"
            help = "Plot the first configuration of each connected component"
            action = :store_true
    end
    return parse_args(s)
end



function main()
    shape_pattern = r"\(\s*([-+]?\d+)\s*,\s*([-+]?\d+)\s*\)x\(\s*([-+]?\d+)\s*,\s*([-+]?\d+)\s*\)"

    parsed_args = parse_commandline()

    shape_str = parsed_args["shape"]
    shape = let m = match(shape_pattern, shape_str)
        if isnothing(m)
            @error "shape should be in format (n11,n12)x(n21,n22)"
            exit(1)
        end
        n11, n12, n21, n22 = [parse(Int, x) for x in m.captures]
        [n11 n21; n12 n22]
    end
    shape_str = "($(shape[1,1]),$(shape[2,1]))x($(shape[1,2]),$(shape[2,2]))"
    output_filepath = isnothing(parsed_args["out"]) ? plotsdir("ice-configurations_$shape_str.pdf") : parsed_args["out"]

    # Make Kagome lattice, ice manifold, and connected components of Hamiltonian
    kagome = KagomeIce.make_kagome_lattice(shape)
    full_hilbert_space_representation = make_ice_manifold(kagome)
    ice_hamiltonian_graph = make_ice_hamiltonian_graph(
        kagome,
        full_hilbert_space_representation
    )
    components = connected_components(ice_hamiltonian_graph)

    if !isdir(dirname(output_filepath))
        mkpath(dirname(output_filepath))
    end

    plot_ice_configurations(
        kagome,
        full_hilbert_space_representation,
        components,
        output_filepath,
        parsed_args["first-only"],
    )
end

main()






            #=
            parities = compute_parity(bvec)
            for ((p, pt1, pt2), h) in zip(parities, kagome.motifs.hexagons)
                x, y = 0.0, 0.0
                for (i, R) in h
                    r = fract2carte(
                        kagome.lattice.supercell,
                        getsitecoord(kagome.lattice.supercell, i) + R
                    )
                    x += r[1]
                    y += r[2]
                end
                x /= 6
                y /= 6
                #for i1 in -1:1, i2 in -1:1
                for i1 in [0], i2 in [0]
                    R = kagome.lattice.supercell.latticevectors * [i1, i2]
                    s = (p == 0 ? "+" : "-")*(pt1 == 0 ? "+" : "-")*(pt2 == 0 ? "+" : "-")
                    annotate!(x + R[1], y + R[2], Plots.text(s, 6))
                end
            end
            =#


                # basis_list = Kagome.BalentsFisherGirvin.generate_basis([5 0; 0 5], UInt128)
    # @show length(basis_list)
    # return

    # for b in basis_list
    #     println(bitstring(b))
    # end


        #=
    function compute_parity(bvec::BR) where {BR<:Unsigned}
        return [
            (
                (sum(Int((bvec >> (i-1)) & 0x1) * (i-1) for (i, _) in hexagon) % 2),
                (sum(Int((bvec >> (i-1)) & 0x1) * (i-1) for (i, _) in hexagon[1:2:end]) % 2),
                (sum(Int((bvec >> (i-1)) & 0x1) * (i-1) for (i, _) in hexagon[2:2:end]) % 2),
            )
                for hexagon in kagome.motifs.hexagons
        ]
    end
    =#




#=

function main()
    # basis_list = Kagome.BalentsFisherGirvin.generate_basis([5 0; 0 5], UInt128)
    # @show length(basis_list)
    # return

    # for b in basis_list
    #     println(bitstring(b))
    # end

    shape = [2 2; -2 4]

    kagome = KagomeIce.make_kagome_lattice(shape)

    n_triangles = length(kagome.nearest_neighbor_triangles)
    n_sites = numsite(kagome.lattice.supercell)

    basis_list = KagomeIce.BalentsFisherGirvin.generate_basis(kagome)
    sort!(basis_list)

    (hilbert_space, spin_operator) = QuantumHamiltonian.Toolkit.spin_system(n_sites, 1//2)
    hamiltonian_terms = KagomeIce.BalentsFisherGirvin.generate_hamiltonian(kagome)

    rng = MersenneTwister(1)

    hamiltonian = let n = length(hamiltonian_terms),
        #magnitude = rand(rng, Float64, n),
        magnitude = 1,
        phase = 1

        amplitude = magnitude .* phase
        h = sum(amplitude  .* hamiltonian_terms)
        h + conj(transpose(h))
    end

    full_hilbert_space_representation = represent_dict(hilbert_space, basis_list)

    hamiltonian_representation = represent(full_hilbert_space_representation, hamiltonian)

    @show dimension(full_hilbert_space_representation)
    ndim = dimension(full_hilbert_space_representation)

    graph = SimpleGraph(ndim)
    for irow in 1:ndim
        ψ = SparseState(Dict(get_row_iterator(hamiltonian_representation, irow)))
        choptol!(ψ, Base.rtoldefault(Float64))
        for (icol, _) in ψ.components
            add_edge!(graph, irow, icol)
        end
    end

    compos = connected_components(graph)

    for (icompo, compo) in enumerate(compos)
        for ivec in compo
            bvec = basis_list[ivec]
            @show ivec
            fig = plot(title="ice compo $icompo config #$ivec", legend=false, size=(400, 400))
            for (isite, ((sitetype, siteuc), sitefc)) in enumerate(kagome.lattice.supercell.sites)
                sitecc = fract2carte(kagome.lattice.supercell, sitefc)
                x = sitecc[1]
                y = sitecc[2]
                xs = Float64[]
                ys = Float64[]
                for i1 in -1:1, i2 in -1:1
                    R = kagome.lattice.supercell.latticevectors * [i1, i2]
                    push!(xs, x + R[1])
                    push!(ys, y + R[2])
                end
                if (bvec >> (isite-1)) & 0x1 == 0x0
                    scatter!(xs, ys, markercolor="black", markerstyle=:circle, markersize=8, alpha=0.2)
                    scatter!([x], [y], markercolor="black", markerstyle=:circle, markersize=8, alpha=1)
                else
                    scatter!(xs, ys, markercolor="white", markerstyle=:circle, markersize=8, alpha=0.2)
                    scatter!([x], [y], markercolor="white", markerstyle=:circle, markersize=8, alpha=1)
                end

            end
            parities = compute_parity(kagome, bvec)

            for ((p, pt1, pt2), h) in zip(parities, kagome.motifs.hexagons)
                x, y = 0.0, 0.0
                for (i, R) in h
                    r = fract2carte(
                        kagome.lattice.supercell,
                        getsitecoord(kagome.lattice.supercell, i) + R
                    )
                    x += r[1]
                    y += r[2]
                end
                x /= 6
                y /= 6
                #for i1 in -1:1, i2 in -1:1
                for i1 in [0], i2 in [0]
                    R = kagome.lattice.supercell.latticevectors * [i1, i2]
                    s = (p == 0 ? "+" : "-")*(pt1 == 0 ? "+" : "-")*(pt2 == 0 ? "+" : "-")
                    annotate!(x + R[1], y + R[2], Plots.text(s, 6))
                end

            end
            plot!(xlim=[-2, 4], ylim=[-2, 4])

            png(format("ice-configuration_{:03d}_{:03d}.png", icompo, ivec))
            break
        end
    end
end

=#
