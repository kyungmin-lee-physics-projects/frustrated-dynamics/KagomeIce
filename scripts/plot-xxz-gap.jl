using DrWatson
quickactivate(joinpath(@__DIR__, ".."))

using MsgPack
using CodecXz
using PyPlot
using PyCall
mpl = pyimport("matplotlib")
using Glob

include(srcdir("levelstatistics.jl"))

function read_database(shape_str::AbstractString)        
    filename_pattern = "gap-sparse-shift-invert_Jperp=(-*,*)_Jz=1.000_Sz=0.0_seed=*_shape=$(shape_str)_spinflip=*.msgpack.xz"
    filepaths = Glob.glob(filename_pattern, datadir("kagome-xxz-disordered-gap/$(shape_str)"))
    database = Dict{Tuple{Float64, Int}, Tuple{Vector{Float64}, Vector{Float64}}}()
    for filepath in filepaths
        open(filepath, "r") do io
            ioc = XzDecompressorStream(io)
            item = MsgPack.unpack(ioc)
            Jperpmax = item["parameter"]["Jperp"][2]
            spinflip = item["sector"]["spinflip"]
            if haskey(database, (Jperpmax, spinflip))
                append!(database[(Jperpmax, spinflip)][1], item["lower_eigenvalue"])
                append!(database[(Jperpmax, spinflip)][2], item["higher_eigenvalue"])
            else
                database[(Jperpmax, spinflip)] = (item["lower_eigenvalue"], item["higher_eigenvalue"])
            end
            close(ioc)
        end
    end
    return database
end

database = read_database("(2,0)x(0,2)")

xs = Float64[]
ys = Float64[]
for ((Jperpmax, spinflip), (lower_energies, higher_energies)) in database
    if spinflip != 1
        continue
    end
    homo = maximum(lower_energies)
    lumo = minimum(higher_energies)
    gap = lumo - homo
    push!(xs, Jperpmax)
    push!(ys, gap)
end

xs, ys = let idx = sortperm(xs)
    (xs[idx], ys[idx])
end


using LsqFit
model(x, p) = 1 .- p[1] .* x
fit = curve_fit(model, xs, ys, [1.0])
@show fit

f(x) = model(x, fit.param)
ys_fit = f.(xs)
fig = PyPlot.figure()
ax = fig.gca()
ax.plot(xs, ys, "o-")
ax.plot(xs, ys_fit, "-")
PyPlot.show()
@show 1/fit.param[1]
fig.savefig("foo.png")