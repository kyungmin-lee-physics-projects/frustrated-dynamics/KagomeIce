using SparseArrays
using LinearAlgebra
using Random
using Plots

using LatticeTools
using QuantumHamiltonian
using KagomeIce

using GraphPlot
using LightGraphs
using PyCall

global const npl = pyimport("numpy.linalg")

shape = [
    2 0;
    0 2
]

kagome = KagomeIce.make_kagome_lattice(shape)

n_triangles = length(kagome.nearest_neighbor_triangles)
n_sites = numsite(kagome.lattice.supercell)

basis_list = KagomeIce.BalentsFisherGirvin.generate_basis(kagome)
sort!(basis_list)

(hilbert_space, spin_operator) = QuantumHamiltonian.Toolkit.spin_system(n_sites, 1//2)
hamiltonian_terms = KagomeIce.BalentsFisherGirvin.generate_hamiltonian(kagome)

rng = MersenneTwister(1)

hamiltonian = let n = length(hamiltonian_terms),
    #magnitude = rand(rng, Float64, n),
    magnitude = 1,
    #phase = cis.(rand(rng, Float64, n) * (2 * pi))
    phase = 1

    amplitude = magnitude .* phase
    h = sum(amplitude  .* hamiltonian_terms)
    h + conj(transpose(h))
end

full_hilbert_space_representation = represent_dict(hilbert_space, basis_list)

hamiltonian_representation = represent(full_hilbert_space_representation, hamiltonian)

@show dimension(full_hilbert_space_representation)
ndim = dimension(full_hilbert_space_representation)

graph = SimpleGraph(ndim)
for irow in 1:ndim
    ψ = SparseState(Dict(get_row_iterator(hamiltonian_representation, irow)))
    choptol!(ψ, Base.rtoldefault(Float64))
    for (icol, _) in ψ.components
        add_edge!(graph, irow, icol)
    end
end

# add spin flips
for irow in 1:ndim
    brow = hamiltonian_representation.hilbert_space_representation.basis_list[irow]
    mask = make_bitmask(n_sites, typeof(brow))
    bcol = mask & (~brow)
    icol = hamiltonian_representation.hilbert_space_representation.basis_lookup[bcol]
    add_edge!(graph, irow, icol)
end


compos = connected_components(graph)

#=
=#


struct FakeIrrepComponent <: AbstractSymmetryIrrepComponent end


# let
#     hsr = represent_dict(hilbert_space, basis_list[compos[1]])
#     rhsr = symmetry_reduce(hsr, spin_flip_symmetry_embedding_irrep_components[1])
#     @show rhsr
# end


function compute_eigenvalues(basis_shortlist::AbstractVector{BR}) where {BR<:Unsigned}
    ComplexType = ComplexF64

    mask = make_bitmask(n_sites, BR)
    @assert count_ones(mask) == n_sites
    spinflip(bvec::BR) = mask & (~bvec)

    hsr = represent_dict(hilbert_space, basis_shortlist)
    HSR = typeof(hsr)
    n_basis = dimension(hsr)

    sector_eigenvalues = Vector{Float64}[]

    for spin_flip_sign in [1, -1]
        basis_mapping_representative = Vector{Int}(undef, n_basis)
        fill!(basis_mapping_representative, -1)
        basis_mapping_amplitude = zeros(Float64, n_basis)

        group_size = 2
        symops_and_amplitudes = [(identity, 1), (spinflip, spin_flip_sign)]
        is_identity = [true, spin_flip_sign == 1]

        reduced_basis_list = BR[]
        sizehint!(reduced_basis_list, n_basis ÷ 2 + n_basis ÷ 4)

        visited = falses(n_basis)

        basis_states = Vector{BR}(undef, group_size)
        basis_amplitudes = Dict{BR, ComplexType}()
        sizehint!(basis_amplitudes, 2)

        for ivec_p in 1:n_basis
            visited[ivec_p] && continue
            bvec = hsr.basis_list[ivec_p]

            compatible = true
            for i in 2:2
                (symop, _) = symops_and_amplitudes[i]
                bvec_prime = symop(bvec)
                if bvec_prime < bvec
                    compatible = false
                    break
                elseif bvec_prime == bvec && !is_identity[i]
                    compatible = false
                    break
                end
                basis_states[i] = bvec_prime
            end
            (!compatible) && continue
            basis_states[1] = bvec

            push!(reduced_basis_list, bvec)

            empty!(basis_amplitudes)
            for i in 1:group_size
                (_, ampl) = symops_and_amplitudes[i]
                bvec_prime = basis_states[i]
                basis_amplitudes[bvec_prime] = ampl
            end
            inv_norm = inv(sqrt(float(length(basis_amplitudes))))
            for (bvec_prime, amplitude) in basis_amplitudes
                ivec_p_prime = hsr.basis_lookup[bvec_prime]
                visited[ivec_p_prime] = true
                basis_mapping_representative[ivec_p_prime] = ivec_p
                basis_mapping_amplitude[ivec_p_prime] = amplitude * inv_norm
            end
        end

        basis_mapping_index = Vector{Int}(undef, n_basis)
        fill!(basis_mapping_index, -1)

        for (ivec_r, bvec) in enumerate(reduced_basis_list)
            ivec_p = hsr.basis_lookup[bvec]
            basis_mapping_index[ivec_p] = ivec_r
        end

        for (ivec_p_prime, ivec_p) in enumerate(basis_mapping_representative)
            (ivec_p <= 0) && continue
            (ivec_p_prime == ivec_p) && continue
            ivec_r = basis_mapping_index[ivec_p]
            basis_mapping_index[ivec_p_prime] = ivec_r
        end

        RHSR = ReducedHilbertSpaceRepresentation{
            HSR,
            FakeIrrepComponent,
            BR,
            ComplexType
        }

        rhsr = RHSR(
            hsr, FakeIrrepComponent(),
            reduced_basis_list, basis_mapping_index, basis_mapping_amplitude,
        )
        @show dimension(rhsr)
        #eigenvalues = eigvals(Hermitian(Matrix(represent(rhsr, hamiltonian))))
        eigenvalues = npl.eigvalsh(Matrix(represent(rhsr, hamiltonian)))
        push!(sector_eigenvalues, eigenvalues)
    end
    return sector_eigenvalues
end

#=

sector_eigenvalues = Vector{Float64}[]
for compo in compos
    append!(sector_eigenvalues, compute_eigenvalues(basis_list[compo]))
end

=#

function get_spacings(values, precision::Integer=8)
    values2 = round.(sort(values); digits=precision)
    spacings = values2[2:end] - values2[1:end-1]
    return spacings
end

function get_ratios(values, precision::Integer=8)
    spacings = get_spacings(values, precision)
    n = length(spacings)
    if n <= 3
        Float64[]
    end

    m = n
    while (m > 1) && (spacings[m] == 0)
        m -= 1
    end

    if m <= 1
        return Float64[]
    end

    s_prev = spacings[m] # nonzero
    ratios = Float64[]
    while m > 1
        m -= 1
        s = spacings[m]
        r = min(s, s_prev) / max(s, s_prev)
        if s != 0
            s_prev = s
        end
        push!(ratios, r)
    end
    return ratios
end

#=

for i in 1:8
    r = get_ratios(sector_eigenvalues[i])
    histogram(r[r .!= 0], bins=100, normalize=true, title="component #$i")
    savefig("sector-complex-(3,-2)x(2,2)-$i.png")
    savefig("sector-complex-(3,-2)x(2,2)-$i.pdf")
end

=#
