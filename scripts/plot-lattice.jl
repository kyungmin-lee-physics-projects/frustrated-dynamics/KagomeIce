using LatticeTools
using QuantumHamiltonian
using Plots
using Formatting

#include("../src/KagomeIce.jl")
using KagomeIce

function main()
    kagome = make_kagome_lattice([3 0; 0 3])

    n_triangles = length(kagome.nearest_neighbor_triangles)
    n_sites = numsite(kagome.lattice.supercell)

    #fig = plot(title="ice configuration #$ivec", legend=false, size=(400, 400))

    sitetypes = [sitetype for ((sitetype, siteuc), sitefc) in kagome.lattice.supercell.sites]
    sitecoords = hcat([fract2carte(kagome.lattice.supercell, sitefc) for ((sitetype, siteuc), sitefc) in kagome.lattice.supercell.sites]...)

    #@show sitetypes
    #@show sitecoords

    fig = scatter(sitecoords[1,:], sitecoords[2,:], series_annotation=sitetypes, legend=false)

    #=
    for tri in kagome.motifs.up_triangles
        xs = Float64[]
        ys = Float64[]
        for (i, R) in tri
            r = fract2carte(kagome.lattice.supercell, getsitecoord(kagome.lattice.supercell, i) + R)
            push!(xs, r[1])
            push!(ys, r[2])
        end
        plot!(Shape(xs,ys), alpha=0.2, color="red")
    end

    for tri in kagome.motifs.down_triangles
        xs = Float64[]
        ys = Float64[]
        for (i, R) in tri
            r = fract2carte(kagome.lattice.supercell, getsitecoord(kagome.lattice.supercell, i) + R)
            push!(xs, r[1])
            push!(ys, r[2])
        end
        plot!(Shape(xs,ys), alpha=0.2, color="blue")
    end
    =#

    for hexa in kagome.motifs.hexagons
        xs = Float64[]
        ys = Float64[]
        for (i, R) in hexa
            r = fract2carte(kagome.lattice.supercell, getsitecoord(kagome.lattice.supercell, i) + R)
            push!(xs, r[1])
            push!(ys, r[2])
        end
        plot!(Shape(xs,ys), alpha=0.2, color="green")
    end


    #=
    for (isite, ((sitetype, siteuc), sitefc)) in enumerate(kagome.lattice.supercell.sites)
        sitecc = fract2carte(kagome.lattice.supercell, sitefc)
        x = sitecc[1]
        y = sitecc[2]
        xs = Float64[]
        ys = Float64[]
        for i1 in -1:1, i2 in -1:1
            R = kagome.lattice.supercell.latticevectors * [i1, i2]
            push!(xs, x + R[1])
            push!(ys, y + R[2])
        end
        if (bvec >> (isite-1)) & 0x1 == 0x0
            scatter!(xs, ys, markercolor="black", markerstyle=:circle, markersize=8, alpha=0.2)
            scatter!([x], [y], markercolor="black", markerstyle=:circle, markersize=8, alpha=1)
        else
            scatter!(xs, ys, markercolor="white", markerstyle=:circle, markersize=8, alpha=0.2)
            scatter!([x], [y], markercolor="white", markerstyle=:circle, markersize=8, alpha=1)
        end


    end
    plot!(xlim=[-1.5, 3.5], ylim=[-1.5, 3.5])
    png(format("kagome-lattice.png"))
    =#

    fig
end

main()
