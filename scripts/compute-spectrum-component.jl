using DrWatson
quickactivate(joinpath(@__DIR__, ".."))

using ArgParse
# using SparseArrays
using LinearAlgebra
using DataStructures

using Random
using Logging
using Printf
using Formatting
using ArgParse

using CodecXz
using MsgPack
using BitIntegers

using Formatting

using LightGraphs
using PyCall

global const npl = pyimport("numpy.linalg")

using LatticeTools
using QuantumHamiltonian
using KagomeIce


struct FakeIrrepComponent <: AbstractSymmetryIrrepComponent end


function compute_eigenvalues(
    hilbert_space::AbstractHilbertSpace,
    hamiltonian::AbstractOperator,
    basis_shortlist::AbstractVector{BR},
) where {BR<:Unsigned}
    ComplexType = ComplexF64

    n_sites = length(hilbert_space.sites)

    mask = make_bitmask(n_sites, BR)
    @assert count_ones(mask) == n_sites
    spinflip(bvec::BR) = mask & (~bvec)

    #new_basis_shortlist = unique(sort(vcat(basis_shortlist, spinflip.(basis_shortlist))))
    new_basis_shortlist = basis_shortlist

    hsr = represent_dict(hilbert_space, new_basis_shortlist)
    HSR = typeof(hsr)
    n_basis = dimension(hsr)

    sector_eigenvalues = Vector{Float64}[]

    for spin_flip_sign in [1, -1]
        basis_mapping_representative = Vector{Int}(undef, n_basis)
        fill!(basis_mapping_representative, -1)
        basis_mapping_amplitude = zeros(Float64, n_basis)

        group_size = 2
        symops_and_amplitudes = [(identity, 1), (spinflip, spin_flip_sign)]
        is_identity = [true, spin_flip_sign == 1]

        reduced_basis_list = BR[]
        sizehint!(reduced_basis_list, n_basis ÷ 2 + n_basis ÷ 4)

        visited = falses(n_basis)

        basis_states = Vector{BR}(undef, group_size)
        basis_amplitudes = Dict{BR, ComplexType}()
        sizehint!(basis_amplitudes, 2)

        for ivec_p in 1:n_basis
            visited[ivec_p] && continue
            bvec = hsr.basis_list[ivec_p]

            compatible = true
            for i in 2:2
                (symop, _) = symops_and_amplitudes[i]
                bvec_prime = symop(bvec)
                if bvec_prime < bvec
                    compatible = false
                    break
                elseif bvec_prime == bvec && !is_identity[i]
                    compatible = false
                    break
                end
                basis_states[i] = bvec_prime
            end
            (!compatible) && continue
            basis_states[1] = bvec

            push!(reduced_basis_list, bvec)

            empty!(basis_amplitudes)
            for i in 1:group_size
                (_, ampl) = symops_and_amplitudes[i]
                bvec_prime = basis_states[i]
                basis_amplitudes[bvec_prime] = ampl
            end
            inv_norm = inv(sqrt(float(length(basis_amplitudes))))
            for (bvec_prime, amplitude) in basis_amplitudes
                ivec_p_prime = hsr.basis_lookup[bvec_prime]
                visited[ivec_p_prime] = true
                basis_mapping_representative[ivec_p_prime] = ivec_p
                basis_mapping_amplitude[ivec_p_prime] = amplitude * inv_norm
            end
        end

        basis_mapping_index = Vector{Int}(undef, n_basis)
        fill!(basis_mapping_index, -1)

        for (ivec_r, bvec) in enumerate(reduced_basis_list)
            ivec_p = hsr.basis_lookup[bvec]
            basis_mapping_index[ivec_p] = ivec_r
        end

        for (ivec_p_prime, ivec_p) in enumerate(basis_mapping_representative)
            (ivec_p <= 0) && continue
            (ivec_p_prime == ivec_p) && continue
            ivec_r = basis_mapping_index[ivec_p]
            basis_mapping_index[ivec_p_prime] = ivec_r
        end

        RHSR = ReducedHilbertSpaceRepresentation{
            HSR,
            FakeIrrepComponent,
            BR,
            ComplexType
        }

        rhsr = RHSR(
            hsr, FakeIrrepComponent(),
            reduced_basis_list, basis_mapping_index, basis_mapping_amplitude,
        )
        @show dimension(rhsr)
        eigenvalues = npl.eigvalsh(Matrix(represent(rhsr, hamiltonian)))
        push!(sector_eigenvalues, eigenvalues)
    end
    return sector_eigenvalues
end


function compute_bfgice(
    shape::AbstractMatrix{<:Integer},
    random_seeds::AbstractVector{<:Integer},
    force::Bool,
)
    kagome = KagomeIce.make_kagome_lattice(shape)

    n_triangles = length(kagome.nearest_neighbor_triangles)
    n_sites = numsite(kagome.lattice.supercell)
    @mylogmsg "Number of sites: $n_sites"

    if n_sites <= 64
        BR = UInt64
    elseif n_sites <= 128
        BR = UInt128
    elseif n_sites <= 256
        BR = UInt256
    elseif n_sites <= 512
        BR = UInt512
    elseif n_sites <= 1024
        BR = UInt1024
    else
        error("Too many sites to be reprented with unsigned integer")
    end
    mask = make_bitmask(n_sites, BR)
    @assert count_ones(mask) == n_sites
    spinflip(bvec::BR) where {BR<:Unsigned} = mask & (~bvec)

    basis_list = KagomeIce.BalentsFisherGirvin.generate_basis(kagome, BR)
    sort!(basis_list)

    @mylogmsg "Creating Hilbert space sector"
    (hilbert_space, spin_operator) = QuantumHamiltonian.Toolkit.spin_system(n_sites, 1//2, BR)

    @mylogmsg "Representing full Hilbert space"
    full_hilbert_space_representation = represent_dict(hilbert_space, basis_list)
    hilbert_space_dimension = dimension(full_hilbert_space_representation)
    @mylogmsg "Hilbert space dimension: $hilbert_space_dimension"

    hamiltonian_terms = KagomeIce.BalentsFisherGirvin.generate_hamiltonian(kagome, BR)

    component_basis_list = let
        uniform_hamiltonian = let
            h = sum(hamiltonian_terms)
            (h + conj(transpose(h)))
        end

        @mylogmsg "Representing Uniform Hamiltonian"
        uniform_hamiltonian_representation = represent(full_hilbert_space_representation, uniform_hamiltonian)

        graph = SimpleGraph(hilbert_space_dimension)
        for irow in 1:hilbert_space_dimension
            ψ = SparseState(Dict(get_row_iterator(uniform_hamiltonian_representation, irow)))
            choptol!(ψ, Base.rtoldefault(Float64))
            for (icol, _) in ψ.components
                add_edge!(graph, irow, icol)
            end
        end

        compos = LightGraphs.connected_components(graph)
        visited = falses(length(basis_list))
        component_basis_list = Vector{BR}[]
        for compo in compos
            if visited[first(compo)]
                continue
            end
            basis_shortlist = basis_list[compo]
            append!(basis_shortlist, spinflip.(basis_shortlist))
            sort!(basis_shortlist)
            unique!(basis_shortlist)
            if length(basis_shortlist) > length(compo)
                for b in basis_shortlist
                    i = full_hilbert_space_representation.basis_lookup[b]
                    visited[i] = true
                end
            elseif length(basis_shortlist) == length(compo)
                visited[compo] .= true
            else
                @assert false
            end
            push!(component_basis_list, basis_shortlist)
        end
        component_basis_list
    end

    for random_seed in random_seeds
        shape_str = "($(shape[1,1]),$(shape[2,1]))x($(shape[1,2]),$(shape[2,2]))"
        parameter_filename = Dict{Symbol, Any}(
            :shape=>shape_str,
            :seed=>random_seed,
        )
        output_filename = savename("spectrum-bfgice-dense", parameter_filename, "msgpack.xz")
        output_filepath = datadir("bfgice", shape_str, output_filename)
        if ispath(output_filepath)
            @mylogmsg "File $output_filepath exists."
            if force
                @mylogmsg "Overwriting."
            else
                @mylogmsg "Skipping."
                continue
            end
        end

        @mylogmsg "Constructing Hamiltonian with random seed $random_seed"

        rng = MersenneTwister(random_seed)
        hamiltonian = let
            n = length(hamiltonian_terms)
            magnitude = randn(rng, Float64, n) # normal distribution, mean=0, std=1
            phase = 1
            amplitude = magnitude .* phase
            h = sum(amplitude .* hamiltonian_terms)
            (h + conj(transpose(h)))
        end

        @mylogmsg "Computing Eigenvalues"
        sector_eigenvalues = []
        for basis_shortlist in component_basis_list
            ev = compute_eigenvalues(hilbert_space, hamiltonian, basis_shortlist)
            push!(sector_eigenvalues, ev)
        end

        @mylogmsg "Saving to $output_filepath"
        mkpath(dirname(output_filepath); mode=0o755)

        open(output_filepath, "w") do io
            ioc = XzCompressorStream(io)
            save_data = OrderedDict(
                "parameter" => OrderedDict(
                    "shape" => [shape[1,1], shape[2,1], shape[1,2], shape[2,2]],
                    "seed" => random_seed,
                ),
                "eigenvalue" => sector_eigenvalues,
            )

            MsgPack.pack(ioc, save_data)
            close(ioc)
        end

    end # for seed
end


function parse_commandline()
    s = ArgParseSettings()
    @add_arg_table! s begin
        "shape"
            arg_type = String
            required = true
        "--seed"
            arg_type = Int
            required = true
            nargs = '+'
        "--force"
            action = :store_true
    end
    return parse_args(s)
end


function main()
    args = parse_commandline()
    shape = KagomeIce.parse_shape(args["shape"])
    compute_bfgice(shape, args["seed"], args["force"])
end


main()
