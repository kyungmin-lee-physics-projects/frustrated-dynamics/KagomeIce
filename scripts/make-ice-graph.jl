using DrWatson
quickactivate(joinpath(@__DIR__, ".."))

import ArgParse
import StatsBase
using LightGraphs
using LatticeTools
using QuantumHamiltonian
using KagomeIce



"""
    make_ice_manifold(kagome)

Return `HilbertSpaceRepresentation` of the kagome lattice
"""
function make_ice_manifold(kagome)
    basis_list = KagomeIce.BalentsFisherGirvin.generate_basis(kagome)
    n_sites = numsite(kagome.lattice.supercell)

    (hilbert_space, spin_operator) = QuantumHamiltonian.Toolkit.spin_system(n_sites, 1//2)
    return represent_dict(hilbert_space, basis_list)
end


"""
    make_ice_hamiltonian_graph(kagome, full_hilbert_space_representation)

Return a graph for ice Hamiltonian
"""
function make_ice_hamiltonian_graph(
    kagome,
    full_hilbert_space_representation::HilbertSpaceRepresentation
)
    hamiltonian_terms = KagomeIce.BalentsFisherGirvin.generate_hamiltonian(kagome)
    hamiltonian = let magnitude = 1, phase = 1
        amplitude = magnitude .* phase
        h = sum(amplitude .* hamiltonian_terms)
        h + conj(transpose(h))
    end
    hamiltonian_representation = represent(full_hilbert_space_representation, hamiltonian)

    ndim = dimension(full_hilbert_space_representation)
    @info "ice manifold dimension = $ndim"

    graph = SimpleGraph(ndim)
    for irow in 1:ndim
        ψ = SparseState(Dict(get_row_iterator(hamiltonian_representation, irow)))
        choptol!(ψ, Base.rtoldefault(Float64))
        for (icol, _) in ψ.components
            add_edge!(graph, irow, icol)
        end
    end
    return graph
end



function parse_commandline()
    s = ArgParse.ArgParseSettings(
        description="""
        Generate a coloring graph file in Pajek .net format.

        The nodes are labeled by their colorings, and the edges are weighted by the Kempe
        loop length.
        """
    )
    ArgParse.@add_arg_table! s begin
        "shape"
            help = "Shape of the lattice, in format (n11,n12)x(n21,n22)"
            arg_type = String
            required = true
        "--out", "-o"
            help = "Name of the output file. Default is kempe-[shape].net"
            arg_type = String
            required = false
            default = nothing
        "--first-only"
            help = "Plot the first configuration of each connected component"
            action = :store_true
    end
    return ArgParse.parse_args(s)
end



function main()
    shape_pattern = r"\(\s*([-+]?\d+)\s*,\s*([-+]?\d+)\s*\)x\(\s*([-+]?\d+)\s*,\s*([-+]?\d+)\s*\)"

    parsed_args = parse_commandline()

    shape_str = parsed_args["shape"]
    shape = let m = match(shape_pattern, shape_str)
        if isnothing(m)
            @error "shape should be in format (n11,n12)x(n21,n22)"
            exit(1)
        end
        n1a, n1b, n2a, n2b = [parse(Int, x) for x in m.captures]
        [n1a n2a;
         n1b n2b]
    end
    shape_str = "($(shape[1,1]),$(shape[2,1]))x($(shape[1,2]),$(shape[2,2]))"
    output_filepath = isnothing(parsed_args["out"]) ? datadir("ice-graph_$shape_str.net") : parsed_args["out"]

    # Make Kagome lattice, ice manifold, and connected components of Hamiltonian
    kagome = KagomeIce.make_kagome_lattice(shape)
    full_hilbert_space_representation = make_ice_manifold(kagome)
    ice_hamiltonian_graph = make_ice_hamiltonian_graph(
        kagome,
        full_hilbert_space_representation
    )

    open(output_filepath, "w") do fp
        println(fp, "*Vertices $(nv(ice_hamiltonian_graph))")
        for (ivec, bvec) in enumerate(full_hilbert_space_representation.basis_list)
            c = extract(basespace(full_hilbert_space_representation), bvec)
            println(fp, "$ivec \"$(join(string(x-1) for x in c.I))\"")
        end
        println(fp, "*Edges")
        for ed in edges(ice_hamiltonian_graph)
            println(fp, "$(ed.src) $(ed.dst)")
        end
        close(fp)
    end

end

main()
